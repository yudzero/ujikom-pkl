<?php

namespace App\Models;

use App\Models\Siswa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class NilaiSiswa extends Model
{
    use HasFactory;

    protected $table = 'nilaisiswa';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public function siswa(){
        return $this->hasOne(Siswa::class, 'nis');
    }
}
