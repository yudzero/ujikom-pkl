<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPeriode extends Model
{
    use HasFactory;

    protected $guarded =['id'];

    protected $table = 'detail_periode';

    public function perusahaan(){
        return $this->belongsTo(Perusahaan::class, 'NoPerusahaan');
    }

    public function periode() {
        return $this->belongsTo(Periode::class, 'id_periode');
    }
}
