<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materikompetensi extends Model
{
    use HasFactory;

    protected $table = 'materikompetensi';

    protected $guarded = ['id'];

    public function element(){
        
        return $this->belongsTo(Elementkompetensi::class);

    }
}
