<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    use HasFactory;

    protected $fillable = ['id_periode', 'periode'];

    protected $table = 'periode';

    public function periode(){
        return $this->hasMany(Periode::class);
    }
}
