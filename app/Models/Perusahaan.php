<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    use HasFactory;

    protected $table = 'perusahaan';
    protected $fillable = ['NoPerusahaan', 'NamaPerusahaan', 'DeskripsiPerusahaan', 'jurusan', 'alamat', 'fax', 'email', 'jumlahmurid'];
    protected $primaryKey = 'NoPerusahaan';
    private $increment = false;

    public function getForeignKey()
    {
        return $this->primaryKey;
    }

    public function siswa(){
        return $this->hasManyThrough(Siswa::class, Pemetaan::class, 'NoPerusahaan', 'nis', 'NoPerusahaan', 'nis');
    }

    public function guru(){
        return $this->belongsTo(Guru::class, 'nip', 'nip');
    }

    public function pemetaan(){
        return $this->hasMany(Pemetaan::class);
    }

    public function jurusan(){
        return $this->belongsTo(Jurusan::class, 'id_jurusan');
    }

    public function detail_periode(){
        return $this->hasMany(DetailPeriode::class);
    }

    public function user() {
        return $this->hasOne(User::class, 'username');
    }

    public function periode(){
        return $this->hasManyThrough(Periode::class, DetailPeriode::class, 'NoPerusahaan', 'id_periode', 'NoPerusahaan', 'id_periode');
    }
}
