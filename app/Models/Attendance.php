<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;
    
    protected $table = 'attendance';

    protected $fillable = ['id_absen', 'aktivitas', 'tanggal', 'nis', 'keterangan'];

    public function siswa(){
        return $this->belongsTo(Siswa::class, 'nis');
    }
}
