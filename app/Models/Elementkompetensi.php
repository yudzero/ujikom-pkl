<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Elementkompetensi extends Model
{
    use HasFactory;

    protected $table = 'elementkompetensi';

    protected $guarded = ['id'];

    public function materi(){
        
    return $this->hasMany(Materikompetensi::class);

    }
}
