<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;

    protected $fillable=['nis','nama', 'kelas', 'email', 'NoTelp', 'id_jurusan', 'id_user', 'Tgllahir', 'Tmplahir','Alamat_Siswa'];

    protected $table = 'siswas';

    protected $primaryKey = 'nis';

    private $increment = false;

    public function getForeignKey()
    {
        return $this->primaryKey;
    }

    public function perusahaan(){
        return $this->hasOneThrough(Perusahaan::class, Pemetaan::class, 'nis', 'NoPerusahaan', 'nis', 'NoPerusahaan');
    }

    

    public function pemetaan(){
        return $this->hasOne(Pemetaan::class, 'nis');
    }

    // public function getCreatedAtAttribute(){
    //     return Carbon::parse($this->attributes['Tgllahir'])
    //     ->translatedFormat('l, d F Y');
    // }

    public function jurusan(){
        return $this->belongsTo(Jurusan::class, 'id_jurusan');
    }

    public function user() {
        return $this->hasOne(User::class, 'username');
    }

    public function nilaisiswa(){
        return $this->hasOne(NilaiSiswa::class, 'nis');
    }

    public function attendance(){
        return $this->hasMany(Attendance::class);
    }
}
