<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Guru;
use App\Models\User;
use App\Models\Siswa;
use App\Models\Jurusan;
use App\Models\Periode;
use App\Models\LogSiswa;
use App\Models\Pemetaan;
use App\Models\Perusahaan;
use App\Imports\UserImport;
use Illuminate\Http\Request;
use App\Models\DetailPeriode;
use App\Imports\TeacherImport;
use Illuminate\Support\Carbon;
use App\Imports\StudentsImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Spatie\Backup\Helpers\Format;

class HubinController extends Controller
{

    public function jurusan_siswa(){
        $jurusan_siswa = "RPL";
        $testProc = DB::select('CALL jurusan_siswa(?)', [$jurusan_siswa]);
        $jurusan = Jurusan::all();

        return view('hubin.siswarpl', [
            'title' =>  'Hubin | Daftar Siswa RPL',
            'titleheader'   =>  'Daftar Siswa RPL',
            'testProc'  =>  $testProc,
            'array'  =>  count($testProc),
            'jurusan' => Jurusan::all()
        ]);
    }
    

    public function pemetaan(Request $request){
        $pemetaan = Pemetaan::with('siswas')->latest()->get();
        
        return view('hubin.peta', [
            'title' =>  'Pemetaan PKL',
            'titleheader'   =>  'Pemetaan',
            'pemetaan'  =>  $pemetaan,
            
        ]);
    }

    // public function tambahpeta(Request $request){
    //     $data = DB::table('pemetaan')->insert([
    //         'id_pendaftaran'    => $request->id_pendaftaran,
    //         'id_periode'    => $request->id_periode,
    //         'NoPerusahaan'    => $request->NoPerusahaan,
    //         'nis'    => $request->nis,
    //         'id_pembimbing'    => $request->id_pembimbing,
    //     ]);

    //     return redirect('/hubin/pemeta');
    // }


    //backup database
    public function backup() {
        Artisan::call('backup:run --only-db --disable-notifications');

        $path = Storage::disk('public')->path("Laravel/*");

        $latest_ctime = 0;
        $latest_filename = '';

        $files = glob($path);
        foreach($files as $file) {
            if(is_file($file) && filectime($file) > $latest_ctime) {
                $latest_ctime = filectime($file);
                $latest_filename = $file;
            }
        }

        return Response::download($latest_filename);
    }

    public function siswaDaftar(){
        $testView = DB::select('SELECT * FROM siswa_terdaftar');

        return view('hubin.siswadaftar', [
            'title' =>  'Hubin | Siswa Dipetakan',
            'titleheader'   =>  'Siswa Dipetakan',
            'testView'  =>  $testView,
            'array' => count($testView)
        ]);
    }

    public function dashboardhubin(){
        $p_siswa = 'diterima';
        $testFunc = DB::select('SELECT data_siswa_terdaftar(?) AS dataSiswa', [$p_siswa]);
        $periode = Periode::all();
        $perusahaan = Perusahaan::all();
        $data = LogSiswa::all();
        $siswa  = Siswa::with('perusahaan')->get();
        return view('hubin.dashboardhubin', [
            'title' =>  'Dashboard | Hubin',
            'titleheader'   =>  'Dashboard',
            'perusahaan'          =>  $perusahaan,
            'siswa'          =>  $siswa,
            'periode'   =>  $periode,
            'data' => $data,
            'testFunc'  =>  $testFunc
        ]);
    }
    
    public function tampilperiode(Request $request){
        $active = $request->periode ? $request->periode : "Januari" ;

       /*  $perusahaan = Perusahaan::whereHas('periode', function ($query) use ( $active ) {
            return $query->where('periode', $active);
        })->get(); */

        $periode = Periode::all();
        $perusahaan = Perusahaan::doesnthave('periode')->get();

        // $perusahaan = Perusahaan::with('periode')->get();
        return view('hubin.pemetaanperiode', [
            'title' =>  'Hubin | Periode',
            'titleheader'   =>  'Pemetaan Periode',
            'perusahaan'    => $perusahaan,
            'periode' => $periode,
            'active' => $active,
            'perusahaan'    => $perusahaan
        ]);
        
    }

    public function insertPeriode(Request $request){
        // dd($request->all());
        foreach($request->checkbox as $key => $value){
            DB::table('detail_periode')->insert([
                'id_periode'    =>  $request->id_periode,
                'NoPerusahaan'    => (int)$value,
            ]);
        }
        return back()->with('success', 'Data Berhasil Di Update!');
    }
    
    // public function index(){
    // //     return view('perusahaanhubin', [
    // //         'title' => 'Daftar Perusahaan'
    // //     ]);
    // // }


    public function hubinperusahaan(){
        $jurusan = Jurusan::all();
        $perusahaan = Perusahaan::all();
        return view('hubin.perusahaanhubin', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan',
            'perusahaan'    =>  $perusahaan,
            'jurusan'   =>  $jurusan
        ]);
    }

    // public function hubinpemetaan(){
    //     return view('hubin.pemetaanpkl', [
    //         'title' =>  'Daftar Perusahaan',
    //         'titleheader'   =>  'Daftar Perusahaan'
    //     ]);
    // }

    public function tampilguru(){
        $guru = Guru::all();
        return view('hubin.guru', [
            'title' =>  'Daftar Guru',
            'titleheader'   =>  'Daftar Guru',
            'guru' => $guru
        ]);
    }

    public function edit_guru(Request $request, $nip){

        try {
             $guru = Guru::where('nip', $request->id);
             $guru->update($request->except(['_token']));
        } catch (\Illuminate\Database\QueryException $exception) {
            if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/guru')->with('error', 'NIP sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/guru')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
        }
       

        return redirect('hubin/guru') ->with('success','Data Berhasil Diubah') ;

    }
    
        public function tambah_guru(Request $request){

            try {
                $validatedData = $request->validate([
                    'nip'   =>  'required|max:11|unique:guru,nip',
                    'nama'  =>  'required|max:255',
                    'NoTelp' => 'required'
                ],[
                    'nip.unique'    =>  "Nip sudah terdaftar di database!",
                    'nama.max:255'  =>  "Nama Terlalu Banyak!"
                ]);
            Guru::create($validatedData);
            } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/guru')->with('error', 'NIP sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/guru')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
            }
        
        return redirect('/hubin/guru') ->with('success','Data Berhasil Ditambah');
    }

    public function tampiljurusan(){
        $jurusan = Jurusan::all();
        return view('hubin.jurusan', [
            'title' =>  'Daftar Jurusan',
            'titleheader'   =>  'Daftar Jurusan',
            'jurusan' => $jurusan
        ]);
    }

    // public function edit_jurusan(Request $request, $id_jurusan){
    //     try {
    //         $jurusan = Jurusan::where('id_jurusan', $request->id);
    //         $jurusan->update($request->except(['_token']));
    //     } catch (\Illuminate\Database\QueryException $exception) {
    //              if ($exception->errorInfo[1] == 1062) {
    //                  // Kode 1062 menunjukkan adanya duplikat data di database.
    //                  return redirect('/hubin/guru')->with('error', 'Jurusan sudah terdaftar di database!');
    //         } else {
    //             // Tangani error lain yang terjadi.
    //             return redirect('/users')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
    //         }
    //         }
        

    //     return redirect('hubin/jurusan') ->with('success','Data Berhasil Diubah') ;

    // }

    public function edit_perusahaan(Request $request, $NoPerusahaan){
        try {
            $perusahaan = Perusahaan::where('NoPerusahaan', $request->id);
            $perusahaan->update($request->except(['_token']));
        } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/guru')->with('error', 'NIP sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/users')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }
        

        return redirect('/hubin/perusahaan') ->with('success','Data Berhasil Diubah') ;

    }



    public function hubineditakunsiswa (){
        return view('hubin.editakunsiswa', [
            'title' =>  'Edit Siswa',
            'titleheader'   =>  'Edit Siswa'
        ]);
    }

    public function siswaterdaftarhubin(){

        
        // $siswa = Pemetaan::where('status', 'diterima')->with('perusahaan', 'jurusan')->get();
        $data = Pemetaan::where('status', 'pending')->with(['siswa', 'guru'])->latest()->get();
        return view('hubin.siswaterdaftarhubin', [
            'title' =>  'Hubin | Siswa Terdaftar',
            'titleheader'   =>  'Daftar Perusahaan',
            'data' => $data
        ]);
    }
    
    public function daftarsiswahubin(){
        $perusahaan = Perusahaan::all();
        $siswa = Siswa::with('perusahaan', 'jurusan')->get();
        $jurusan = Jurusan::all();
        
        return view('hubin.daftarsiswahubin', [
            'title' =>  'Hubin | Daftar Siswa',
            'titleheader'   =>  'Daftar Siswa',
            "siswa" => $siswa,
            "perusahaan"    => $perusahaan,
            "jurusan"   => $jurusan
            
        ]);
    }

    public function cetaksurat(){
        $perusahaan = Perusahaan::all();

        
        $data = Perusahaan::all();

        return view('hubin.cetaksurat', [
            'title' =>  'Hubin | Cetak Surat',
            'titleheader'   =>  'Cetak Surat',
            'perusahaan'    =>  $perusahaan,
            'data'          =>  $data
        ]);
    }

    //fungsi pemetaan siswa
    public function pemetaansiswa(Request $request){
         

        

        $active = $request->periode ? $request->periode : "Januari" ;

        /* $data = Perusahaan::with(['pemetaan' => function($q){
            return $q->where('status', 'pending')->with(['siswa']);
        }, 'periode' => function ($query) use ( $active ) {
            return $query->where('periode', $active);
        }, 'guru'])->latest()->get(); */

        $perusahaan = Perusahaan::whereHas('periode', function ($query) use ( $active ) {
                return $query->where('periode', $active);
        })->whereHas('pemetaan', function ($query){
                return $query->where('status', "pending");
        })->with(['periode' => function($query) use ($active){
            return $query->where('periode', $active);
        }, 'siswa' => function($query){
            return $query->whereHas('pemetaan', function($query){
                return $query->where("status", "pending");
            });
        }])->get(); 

        /* dd($perusahaan); */

        $periode = Periode::all();

        

        // $perusahaan = Perusahaan::where('status', ['diterima', 'ditolak'])->with('siswa')->get();
        // $data = Perusahaan::has('siswa')->has('guru')->with('siswa', 'guru')->latest()->get();

        // $pemetaan = DB::table('pemetaan')
        // ->join('perusahaan','perusahaan.NoPerusahaan','=','pemetaan.NoPerusahaan')
        // ->where('perusahaan.NamaPerusahaan')
        // ->get();
        // $data = Pemetaan::where('status', 'pending')->with(['siswa', 'guru'])
        // ->latest()
        // ->orderBy('NoPerusahaan')
        // ->groupBy('NoPerusahaan')
        // ->get();

        $guru  = Guru::all();

        return view('hubin.pemetaansiswa', [
            'title' =>  'Hubin | Pemetaan Siswa',
            'titleheader'   =>  'Pemetaan Siswa',
            'guru'  => $guru,
            'periode'   =>  $periode,
            'active'    =>  $active,
            'perusahaan'    => $perusahaan
            
            // 'perusahaan'    =>  $perusahaan
        ]);
    }


    public function terimaSiswa(Request $request) {

       /* dd(Pemetaan::where('nis', $request->nis)->where('status', 'pending')->get(), $request->nis); */

        Pemetaan::where('nis', $request->nis)->where('status', 'pending')->update([
            'nip'   => $request->nip,
            'id_periode'    =>  $request->id_periode,
            'status' => 'diterima',
            
        ]);

        

        
        return back()->with('success', 'Siswa diterima');
    }

    public function tolakSiswa(Request $request) {
        // $ditolak = Pemetaan::where('nis', $request->nis)->where('status', 'pending')->update([
        //     'status' => 'ditolak',
        // ]);

        Pemetaan::where('nis', $request->nis)->where('status', 'pending')->delete();

        // if ($ditolak) {
        //     $ditolak->delete();
        // }

        // $pemetaan = Pemetaan::where('nis', $request->nis)->first();

        // $perusahaan = Perusahaan::where('NoPerusahaan', $pemetaan->NoPerusahaan)->first();
        // $kuota = $perusahaan->kuota;
        // $perusahaan->kuota = $kuota -= 1;
        // $perusahaan->save();
        
        return back()->with('error', 'Siswa Ditolak!');
    }

    public function pemetaanguru(){
        
        $data = Perusahaan::all();
        $guru = Guru::all();
        return view('hubin.pemetaanguru', [
            'title' =>  'Hubin | Pemetaan Guru',
            'titleheader'   =>  'Pemetaan Guru',
            'data'  =>  $data,
            'guru'    =>  $guru
        ]);
    }

    public function updateguru(Request $request, Perusahaan $perusahaan){
        /* $perusahaan->update([
            'nip' => $request->nip
        ]);
        dd($request->nip); */
        // dd($request->all());

        try {
            Perusahaan::where('NoPerusahaan', $perusahaan->NoPerusahaan)->update([
            'nip'   => $request->nip
        ]);
        } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/guru')->with('error', 'NIP sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/guru')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }
        

        return  redirect()->back();
    }

    // public function importdata(Request $request){
	// 	Excel::import(new UserImport, $request->file('file'));

    //     return back()->with('success','Excel File Berhasil Diimport!');
    // }
    public function importdata(Request $request)
    {

        try {
            // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_account', $nama_file);

        // import data
        Excel::import(new UserImport, public_path('/file_account/' . $nama_file));
        } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/importdata')->with('error', 'Data sudah terdaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/importdata')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }
        


        // alihkan halaman kembali
        return back()->with('success', 'Data Berhasil Di Upload!');
    }

    public function tampilimport(){
           $user = User::all();
        return view('hubin.upload',[
            'user'  => $user,
            'title' =>  'Import Data User',
            'titleheader'   =>  'Import Data User'
        ]);
    }

    public function tampilimportguru(){
           $guru = Guru::all();
        return view('hubin.importguru',[
            'guru'  => $guru,
            'title' =>  'Import Data Guru',
            'titleheader'   =>  'Import Data Guru'
        ]);
    } 

    public function importdataguru(Request $request){

        try {
            $file = $request->file('file');
		    Excel::import(new TeacherImport, $file);
        } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/importguru')->with('error', 'Data sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/importguru')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }
        

        return back()->with('success','Excel File Berhasil Diimport!');
    }

    public function cetakmurid($NoPerusahaan){
        $data = Perusahaan::find($NoPerusahaan);

        $pdf = PDF::loadView('hubin.surat', [
            'data'  => $data
        ]);

        return $pdf->setPaper('a4', 'portrait')->download('suratpengajuan.pdf');
    }

    public function suratpdf($NoPerusahaan){
        $data = Perusahaan::find($NoPerusahaan)->with(['pemetaan' => function ($query){
            return $query->where('status', 'pending')->with('siswa.jurusan');
        }]);

        /* $perusahaan = Perusahaan::where('pemetaan', function($query){
            return $query->where('status', 'pending');
        })->with('siswa.jurusan')->get(); */

        return view('hubin.surat', [
            'data'  =>  $data
        ]);
    }


    public function tambahdata(Request $request, Perusahaan $perusahaan){
        
            try {
                DB::table('perusahaan')->insert([
                'NoPerusahaan' => $request->NoPerusahaan,
                'nama' => $request->nama,
                'DeskripsiPerusahaan'   =>  $request->DeskripsiPerusahaan,
                'jurusan'   =>  $request->jurusan,
                'alamat' => $request->alamat,
                'email' => $request->email,
                'kuota' => 0,
                'status'    =>  true,
                'fax' => $request->fax,
                'maps' => $request->maps,
                'id_jurusan'    =>  $request->id_jurusan
        ]);
            } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/perusahaan')->with('error', 'NoPerusahaan sudah terdaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/perusahaan')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }

        

        

        return redirect('/hubin/perusahaan') ->with('success','Data Berhasil Ditambah');
    }

    public function tambahdatasiswa(Request $request){

        try {
            DB::table('siswas')->insert([
            'nis'   =>  $request->nis,
            'nama'  =>  $request->nama,
            'id_jurusan'    =>  $request->id_jurusan,
            'kelas' => $request->kelas,
            'Tgllahir' => $request->Tgllahir,
            'Tmplahir' => $request->Tmplahir,
            'Alamat_Siswa' => $request->Alamat_Siswa,
            'NoTelp' => $request->NoTelp,
            'email'  =>  $request->email
        ]);
        } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/siswa')->with('error', 'NIS sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/siswa')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }

        



        return redirect('/hubin/siswa')->with('success','Berhasil Menambah Data!');
    }

    public function hapusjurusan($id){
        DB::table('jurusan')->where('id_jurusan', $id)->delete();

        return redirect('/hubin/jurusan')->with('info','Data berhasil Dihapus');
    }

    public function hapusguru($id){
        DB::table('guru')->where('nip', $id)->delete();

        return redirect('/hubin/guru')->with('info','Data berhasil Dihapus');
    }

    public function hapussiswa($id){
        DB::table('siswas')->where('nis', $id)->delete();

        return redirect('/hubin/siswa')->with('info','Data berhasil Dihapus');
    }

    public function hapusperusahaan($id){
        DB::table('perusahaan')->where('NoPerusahaan', $id)->delete();

        return redirect('/hubin/perusahaan')->with('info','Data berhasil dihapus');
    }

    public function updatesiswa(Request $request, $nis){

        try {
            $siswa = Siswa::with('pemetaan')->where('nis', $request->id);
            $siswa->update($request->except(['_token']));
        } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/siswa')->with('error', 'NIS sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/siswa')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }
        

        return back()->with('success','Data berhasil Diubah');
    }

    public function tampilimportsiswa(){
        return view('hubin.importsiswa', [
            'title' =>  'Hubin | Import Siswa',
            'titleheader'   =>  'Import Data Siswa'
        ]);
    }


    // mengimport excel siswa
    public function importsiswa(Request $request){

        try {
            $file = $request->file('file');
		    Excel::import(new StudentsImport, $file);
        } catch (\Illuminate\Database\QueryException $exception) {
                 if ($exception->errorInfo[1] == 1062) {
                     // Kode 1062 menunjukkan adanya duplikat data di database.
                     return redirect('/hubin/importsiswa')->with('error', 'Data sudah tedaftar di database!');
            } else {
                // Tangani error lain yang terjadi.
                return redirect('/hubin/importsiswa')->with('error', 'Terjadi kesalahan: '.$exception->getMessage());
            }
         }

        

        return back()->with('success','Excel File Berhasil Diimport!');

    }
    
    //mencetak rekap perusahaan
    public function rekapPerusahaan(Request $request){
        $data = DetailPeriode::whereHas('perusahaan', function($q) use($request){
            $q->where('id_periode', $request->id_periode);
        })->get();
        // dd($data);
        $pdf = PDF::loadview('hubin.rekap-perusahaan', [
            'data'  => $data
        ]);

        return $pdf->setPaper('a4', 'portrait')->download('rekap-perusahaan.pdf');
    }


    // mencetak rekap pemetaan
    public function rekapPemetaan(Request $request){
        $data = Pemetaan::where('id_periode', $request->id_periode)->with(['siswa', 'perusahaan', 'guru'])->get();

        
        $pdf = PDF::loadview('hubin.rekap-pemetaan', [
            'data'  => $data
        ]);

        return $pdf->setPaper('a4', 'portrait')->download('rekap-pemetaan.pdf');
    }

    public function filterPeriode(Request $request){
        foreach($request->nama as $key => $value){
            DB::table('detail_periode')->insert([
                'id_periode'    =>  $request->id_periode,
                'NoPerusahaan'    => $value,
            ]);
        }
        return back()->with('success', 'Data Berhasil Di Update!');
    }
    
    public function logSiswa(){
        $data = LogSiswa::all();
        $mytime = Carbon::now();
        // $result = Carbon::createFromFormat('d/m/y',$mytime)->diffForHumans();

        return view('hubin.logSiswa', [
            'title' =>  'Hubin | History Siswa',
            'titleheader'   =>  'History Siswa',
            'data'  =>  $data
        ]);
    }
}