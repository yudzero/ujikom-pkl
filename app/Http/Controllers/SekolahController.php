<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Guru;
use App\Models\Siswa;
use App\Models\Pemetaan;
use App\Models\Attendance;
use App\Models\NilaiSiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;


class SekolahController extends Controller
{

    // public function charts(){
    //     $groups = DB::table('attendance')->select('keterangan', DB::raw('count(*) as total'))
    //                 ->groupBy('keterangan')->pluck('total', 'keterangan')->all();
    //                 for($i=0; $i<=count($groups); $i++){
    //                     $colours[]  =   '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
    //                 }

    //                 $chart = new Chart;
    //                 $chart->labels = (array_keys($groups));
    //                 $chart->dataset = (array_values($groups));
    //                 $chart->colours = $colours;

    //                 return view('pembimbing-sekolah.berandasekolah', [
    //                     'chart' =>  $chart
    //                 ]);

    // }

    // fungsi dashboard dan chart
    public function dashboardsekolah(){
                    
        $siswa = Pemetaan::where('nip', auth()->user()->username)->with('siswa', 'perusahaan')->get();
        $siswaCount = Attendance::count();
        $pembimbing = Pemetaan::where('nip', auth()->user()->username)->get(['nis']);
        
        $chart = DB::table('attendance')->select('keterangan', DB::raw('count(*) as jumlah'))->whereIn('nis', $pembimbing)->whereBetween('tanggal', [Carbon::now()->startOfWeek(Carbon::SUNDAY)->format('Y-m-d'), Carbon::now()->endOfWeek(Carbon::SATURDAY)->format('Y-m-d')])->groupBy('keterangan')->get();
        $guru = Guru::where('nip', auth()->user()->username)->first();
        
        $absenTepat = (Attendance::where('keterangan', 'HADIR')->count() / $siswaCount) * 100;
        $absenSakit = (Attendance::where('keterangan', 'SAKIT')->count() / $siswaCount) * 100;
        $absenIzin = (Attendance::where('keterangan', 'IZIN')->count() / $siswaCount) * 100;
        $absenAlpha = (Attendance::where('keterangan', 'ALPHA')->count() / $siswaCount) * 100;
        /* dd($absenTepat, $absenSakit, $absenIzin, $absenAlpha); */
        return view('pembimbing-sekolah.berandasekolah', [
            'title' => 'Dashboard | Pembimbing Sekolah',
            'titleheader' => 'Dashboard',
            // 'sekolah'   => $sekolah,
            'siswa' =>  $siswa,
            'guru'  =>  $guru,
            'chart' => $chart,
            'chartData' => [
                'sakit' => $absenSakit,
                'hadir' => $absenTepat,
                'izin' => $absenIzin,
                'alpha' => $absenAlpha,
            ]
        ]);
    }
    public function evaluasipkl(){
        return view('pembimbing-sekolah.evaluasipkl', [
            'title' =>  'Dashboard | Evaluasi PKL',
            'titleheader'   =>  'Evaluasi PKL'
        ]);
    }

    public function viewsikapsiswa($nis){
        $sekolah = Guru::where('nip', auth()->user()->username)->first();
        // $siswa = Siswa::find($nis);
        $data = NilaiSiswa::with('siswa')->where('nis', $nis)->first();
        $siswa = Siswa::where('nis', $nis)->with('jurusan')->first();
        return view('pembimbing-sekolah.viewsikapsiswa', [
            'title' => 'Dashboard | Nilai Sikap Siswa',
            'titleheader' => 'Nilai Sikap Siswa',
            'data'  =>  $data,
            'siswa' =>   $siswa,
            'sekolah'   =>  $sekolah
            
        ]); 
    }

    public function daftrasiswasekolah(){

       
        // $guru = Guru::where('nip', auth()->user()->username)->with('siswa.jurusan')->first();
        $data = Pemetaan::where('nip', auth()->user()->username)->with('siswa', 'perusahaan')->get();
        return view('pembimbing-sekolah.daftarsiswasekolah',[
            'title' => 'Dashboard | Daftar Siswa',
            'titleheader' => 'Daftar Siswa',
            'data'  =>  $data,
            // 'guru'   =>  $guru
        ]);
    }
    
    
}
