<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ElementController extends Controller
{

    public function element(){
        $data = Jurusan::all();
        return view('hubin.tambahelement',[
            'title' =>  'Hubin | Tambah Data Element',
            'titleheader'   =>  'Tambah Data Element',
            'data'  => $data
        ]);
    }

    public function insertelement(Request $request){
        DB::table('elementkompetensi')->insert([
            'element'  => $request->element,
            'id_jurusan'  => $request->id_jurusan,
            'tahun_ajar'  => $request->tahun_ajar
        ]);

        return redirect('/hubin/element')->with('success', 'Berhasil!');
    }
}
