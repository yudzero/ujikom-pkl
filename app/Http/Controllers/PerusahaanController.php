<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\NilaiSiswa;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

class PerusahaanController extends Controller
{
    public function pembimbingperusahaan(){
        $siswa = Siswa::all();
        $perusa = Perusahaan::where('NoPerusahaan', auth()->user()->username)->first();
        // dd($perusa);
        $perusahaan = Perusahaan::where('NoPerusahaan', Auth::user()->username)->first();
        return view('pembimbing-perusahaan.pembimbing-perusahaan', [
            'title' =>  'Dashboard | Pembimbing Perusahaan',
            'titleheader'   =>  'Pembimbing Perusahaan',
            'siswa' => $siswa,
            'perusahaan'    =>  $perusahaan,
            'perusa'    =>  $perusa
        ]);
    }

    public function detaildata(){
        return view('pembimbing-perusahaan.detail-data', [
            'title' => 'Detail Data',
            'titleheader' => 'Detail Data'
        ]);
    }

    public function daftarsiswa(){
        $perusahaan = Perusahaan::where('NoPerusahaan', auth()->user()->username)->with('siswa', function($query){
            return $query->with(['jurusan', 'nilaisiswa']);
        })->first();
        $perusa = Perusahaan::where('NoPerusahaan', auth()->user()->username)->first();
       /*  $nilai = NilaiSiswa::where('nis', auth()->user()->username)->first(); */

        /* dd($perusahaan->siswa[0]->nilaisis); */
        // dd($perusahaan);
        // $siswa = Siswa::all();
        
        return view('pembimbing-perusahaan.daftarsiswa', [
            'title' => 'Daftar Siswa',
            'titleheader' => 'Daftar Siswa',
            'siswa' => $perusahaan,
            'perusa'    =>  $perusa,
            /* 'nilai' =>  $nilai */
        ]);
    }

    public function berinilai($nis){

        $perusa = Perusahaan::where('NoPerusahaan', auth()->user()->username)->first();
        $siswa = Siswa::find($nis);
        return view('pembimbing-perusahaan.berinilai', [
            'title' => 'Beri Nilai',
            'titleheader' => 'Beri Nilai',
            'siswa' =>  $siswa,
            'perusa'    =>  $perusa
        ]);
    }

    public function postnilai(Request $request){
        
        // $data = $this->validate($request, [
        //     'pkp'   =>  'required|numeric',
        //     'ki'   =>  'required|numeric',
        //     'mm'   =>  'required|numeric',
        //     'kreativitas'   =>  'required|numeric',
        //     'kt'   =>  'required|numeric',
        //     'dt'   =>  'required|numeric',
        //     'pk'   =>  'required|numeric',
        //     'kmassk'   =>  'required|numeric',
        //     'kk'   =>  'required|numeric',
        //     'ik'   =>  'required|numeric',
        //     'ppt'   =>  'required|numeric',
        //     'pak'   =>  'required|numeric'
        // ]);
         $request->validate([
            'pkp'=> 'required|alpha|max:1',
            'ki'=> 'required|alpha|max:1',
            'mm'=> 'required|alpha|max:1',
            'kreativitas'=> 'required|alpha|max:1',
            'kt'=> 'required|alpha|max:1',
            'dt'=> 'required|alpha|max:1',
            'pk'=> 'required|integer|between:10,100',
            'kk'=> 'required|integer|between:10,100',
            'kk'=> 'required|integer|between:10,100',
            'kmm'=> 'required|integer|between:10,100',
            'ik'=> 'required|integer|between:10,100',
            'kmassk'=> 'required|integer|between:10,100',
            'ppt'=> 'required|integer|between:10,100',
            'pak'=> 'required|integer|between:10,100'
        ]);
        
        NilaiSiswa::create([
            'pkp'   =>  $request->pkp,
            'ki'   =>  $request->ki,
            'mm'   =>  $request->mm,
            'kreativitas'   =>  $request->kreativitas,
            'kt'   =>  $request->kt,
            'dt'   =>  $request->dt,
            'pk'   =>  $request->pk,
            'kmm'   =>  $request->kmm,
            'kmassk'   =>  $request->kmassk,
            'kk'   =>  $request->kk,
            'ik'   =>  $request->ik,
            'ppt'   =>  $request->ppt,
            'pak'   =>  $request->pak,
            'nis'   => $request->nis
        ]);

        return redirect('/dashboard/pembimbingperusahaan')->with('success', 'Berhasil Memberi Nilai!');
    }

    public function editNilai(Request $request, $nis){
        $perusa = Perusahaan::where('NoPerusahaan', auth()->user()->username)->first();
        $siswa = Siswa::where('nis', $request->nis)->first();
        $nilai = NilaiSiswa::where('nis', $request->nis)->first();

        return view('pembimbing-perusahaan.editnilai', [
            'siswa' => $siswa,
            'nilai' =>  $nilai,
            'perusa'    =>  $perusa,
            'title' => 'Pembimbing Perusahaan | Edit Nilai',
            'titleheader'   =>  'Edit Nilai'
        ]);
    }

    public function edit_nilai(Request $request){
        $jurusan = NilaiSiswa::where('nis', $request->nis);
        $jurusan->update($request->except(['_token']));

        
        return redirect('dashboard/pembimbingperusahaan') ->with('success','Data Berhasil Diubah') ;

    }
}
