<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Elementkompetensi;
use Illuminate\Support\Facades\DB;

class MateriController extends Controller
{
    public function pemetaankompetensi(){
        $data = Elementkompetensi::all();
        return view('hubin.pemetaankompetensi', [
            'data'  =>  $data,
            'title' =>  'Hubin | Pemetaan Kompetensi',
            'titleheader'   =>  'Pemetaan Kompetensi PKL',
      
        ]);
    }

    public function insertmateri(Request $request){
        DB::table('materikompetensi')->insert([
            'id_element'    => $request->id_element,
            'NamaMateri'    => $request->NamaMateri,
            'pelaksanaan_pembelajaran'    => $request->pelaksanaan_pembelajaran,
            'indikator_keberhasilan'    => $request->indikator_keberhasilan,
            'catatan'    => $request->catatan
        ]);

        return redirect('/hubin/pemetaankompetensi')->with('success', 'Berhasil!');
    }
}
