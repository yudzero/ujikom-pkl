<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\User;
use App\Models\Siswa;
use App\Models\Jurnal;
use App\Models\NilaiSiswa;
use App\Models\Pemetaan;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
    
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\Auth;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class SiswaController extends Controller
{
    public function jurnalsiswa(){
         $siswas = Siswa::where('nis', auth()->user()->username)->first();
        return view('siswa.jurnalsiswa', [
            'title' =>  'Siswa | Jurnal Siswa',
            'titleheader'   =>  'Jurnal Siswa',
            'siswas'    =>  $siswas
        ]);
    }

    public function sikapsiswa(){
        $siswas = Siswa::where('nis', auth()->user()->username)->first();

        $data = NilaiSiswa::all()->first();
        return view('siswa.sikapsiswa', [
            'title' =>  'Siswa | Sikap Siswa',
            'titleheader'   =>  'Sikap Siswa',
            'data'  => $data,
            'siswas'    =>  $siswas
        ]);
    }

    public function daftarindustrisiswa(){
        $detailPerusahaan = Perusahaan::where('NoPerusahaan')->first();
        $perusahaan='';
        $pemetaan = Pemetaan::where('nis', auth()->user()->nis)->first();
        if ($pemetaan) {
            $perusahaan = Perusahaan::where('NoPerusahaan', $pemetaan->NoPerusahaan)->with('jurusan')->get();
        } else {
            $perusahaan = Perusahaan::has('jurusan')->where('kuota', '<', '3')->where('status', true)->with('jurusan')->get();
        }
        // $data   = Pemetaan::has('jurusan')->has('perusahaan')->with(['jurusan', 'perusahaan'])->get();
        $siswa = Siswa::all();
         $siswas = Siswa::where('nis', auth()->user()->username)->first();
        return view('siswa.daftarindustri-siswa', [
            'title' =>  'Siswa | Daftar Industri',
            'titleheader'   =>  'Daftar Industri',
            'perusahaan'    =>  $perusahaan,
            'siswa' =>  $siswa,
            'detailPerusahaan'  =>  $detailPerusahaan,
            'siswas'  =>  $siswas
        ]);
    }



    public function profilsiswa(Siswa $siswa){
        $siswa = Siswa::all()->first();
        return view('siswa.profilsiswa', [
            'title' =>  'Siswa | Profil Siswa',
            'titleheader'   =>  'Profil Siswa',
            'siswa'          =>  $siswa
        ]);
    }

    public function updateprofil(Request $request, Siswa $siswa){
        $attr = $request->validate([
            'NamaSiswa' =>  ['string', 'min:3', 'max:191', 'required'],
            'email' =>  ['email', 'string', 'min:3', 'max:191', 'required'],
            'NoTelp' =>  ['string', 'min:3', 'max:191', 'required'],
        ]);

        $siswa->update($attr);
 
            return back()->with('successs', 'Profil Sukses Di Update');
    }

    public function berandasiswa(){
        $siswa = Siswa::where('nis', auth()->user()->username)->first();

        return view('siswa.berandasiswa', [
            'title' =>  'Siswa | beranda Siswa',
            'titleheader'   =>  'beranda Siswa',
            'siswa' =>  $siswa
        ]);

        
    }

    public function absenhadir(){
        return view('siswa.absen-hadir', [
            'title' => 'Siswa | Absen',
            'titleheader' => 'Absen'
        ]);
    }

    public function formdaftar(){
        return view('siswa.daftarpkl', [
            'title' => 'Siswa | Daftar PKL',
            'titleheader' => 'Daftar'
        ]);
    }

    public function isijurnal(){
        $jurnal = Attendance::with('siswa')->first();
        $check = Attendance::where('nis', auth()->user()->username)->where('tanggal', date('Y-m-d'))->exists();
         $siswas = Siswa::where('nis', auth()->user()->username)->first();
        
        return view('siswa.isijurnal', [
            'title' => 'Siswa | Isi Jurnal',
            'titleheader' => 'Pengisian Jurnal Prakerin',
            'jurnal'    => $jurnal,
            'check' => $check,
            'siswas'    =>  $siswas
        ]);
    }

    public function insertJurnal(Request $request, Attendance $attendance){
        // $attr = $request->validate([
        //     'aktivitas' =>  ['required', 'string', 'min:3', 'max:1000'],
        // ]);

            /* dd($request->all()); */
        $siswa = Siswa::where('nis', auth()->user()->username)->first();

        // DB::table('attendance')->insert([
        //     'aktivitas' =>  $request->aktivitas,
        //     'tanggal'   =>  date('Y-m-d'),
        //     'nis'   =>  $siswa->nis,
        //     'keterangan'    =>  'HADIR'
        // ]);

        
        // $waktu = '10:10:10';
        /* $keterangan = Attendance::where('keterangan'); */
        if ($request->keterangan == 'hadir') {
            Attendance::create([
                'id_absen' => 1,
            'aktivitas' =>  $request->aktivitas,
            'tanggal'   =>  date('Y-m-d'),
            'nis'   =>  $siswa->nis,
            'keterangan'    =>  'HADIR'
            ]);
        }else{
            Attendance::create([
            'tanggal'   =>  date('Y-m-d'),
            'nis'   =>  $siswa->nis,
            'keterangan'    =>  $request->keterangan1
            ]);
        }

        // if(date('H:i:s') < $waktu) {
        //     Attendance::create([
        //         'aktivitas' =>  $attr['aktivitas'],
        //         'tanggal'   =>  date('Y-m-d'),
        //         'nis'   =>  $siswa->nis,
        //         'keterangan' => 'HADIR'
        //     ]);
        // } elseif(date('H:i:s') > $waktu) {
        //     Attendance::create([
        //         'aktivitas' =>  $attr['aktivitas'],
        //         'tanggal'   =>  date('Y-m-d'),
        //         'nis'   =>  $siswa->nis,
        //         'keterangan' => 'TELAT'
        //     ]);
        // }
            
        return back()->with('success', 'Anda Sudah Absen!');
    }

    public function absentidakhadir(){
        return view('siswa.absen-tidakhadir', [
            'title' => 'Siswa | Absensi',
            'titleheader' => 'Absensi'
        ]);
    }

    public function siswajurnal(Request $request){
        DB::table('siswas')->insert([
            'aktivitas'  =>  $request->aktivitas,
            'tanggal' => $request->tanggal,
            'divisi' => $request->divisi,
            'waktumulai' => $request->waktumulai,
            'waktuselesai' => $request->waktuselesai,
            'foto' => $request->foto
        ]);

        return redirect('/hubin/perusahaan');
    }

    public function masukpemetaan(Request $request){
        
        // dd($request->all());
        $nip = Perusahaan::where('NoPerusahaan', $request->NoPerusahaan)->first();
        DB::table('pemetaan')->insert([
            'id_periode' => $request->id_periode,
            'id_pendaftaran' => $request->nis,
            'NoPerusahaan' => $request->NoPerusahaan,
            'nis' => $request->nis,
            'nip'   => $nip->nip
        ]);

        // dd($request);
        

        $perusahaan = Perusahaan::where('NoPerusahaan', $request->NoPerusahaan)->first();
        $kuota = $perusahaan->kuota;

        $perusahaan->kuota = $kuota += 1;

        if ($perusahaan->kuota == 3) {
            $perusahaan->status = false;
        }
        
        $perusahaan->save();

        return redirect('/siswa/daftarpkl')-> with('success','Anda Berhasil Mendaftar');
    }
    
}
