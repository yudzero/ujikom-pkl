<?php

namespace App\Imports;

use App\Models\Siswa;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        return new Siswa([
            'nis' => $row[0],
            'id_jurusan' => $row[1],
            'nama' => $row[2],
            'kelas' => $row[3],
            'Tgllahir' => $row[4],
            'Tmplahir' => $row[5],
            'Alamat_Siswa' => $row[6],
            'NoTelp' => $row[7],
            'email' => $row[8]
        ]);
    }
}
