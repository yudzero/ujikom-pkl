@extends('layouts.hubin.main')
@section('content')
    <div class="container">

        <table class="table" id="myTable">
  <thead>
    <tr>
      <th scope="col">NIS</th>
      <th scope="col">Nama</th>
      <th scope="col">Kelas</th>
    </tr>
  </thead>
  <tbody>
    @for ($i = 0; $i < $array; $i++)
    <tr>
      <th scope="row"><span class="badge bg-success">{{ $testView[$i]->nis }}</span></th>
      <td>{{ $testView[$i]->nama }}</td>
      <td>{{ $testView[$i]->kelas }}</td>
    </tr>
    @endfor
  </tbody>
</table>
    </div>
@endsection