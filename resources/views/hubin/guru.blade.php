@extends('layouts.hubin.main')
@section('content')
<style>
  thead, th {text-align: center;}
.table thead,
.table th {text-align: center;}
#myTable thead,
#myTable th {text-align: center;}

</style>
<section>

    

    <div class="container">
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
    @endforeach
    </div>

    <button class="btn btn-primary ml-5" data-toggle="modal" data-target="#tambah-perusahaan"> 
        Tambah Data    
    </button><br><br>

    <div class="modal fade" id="tambah-perusahaan" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="/tambah/guru" method="POST" class="needs-validation" novalidate>
                @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="inputPassword5" class="form-label ">NIP</label>
                    <input type="number"  name="nip" id="inputPassword5" class="form-control needs-validation"
                        aria-describedby="passwordHelpBlock" required>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">Harap isi NIP.</div>
                    <label for="inputPassword5" class="form-label ">Nama</label>
                    <input type="text"  name="nama" id="inputPassword5" class="form-control needs-validation"
                        aria-describedby="passwordHelpBlock" required>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">Harap isi Nama.</div>
                    <label for="inputPassword5" class="form-label ">No Telepon</label>
                    <input type="number"  name="NoTelp" id="inputPassword5" class="form-control needs-validation"
                        aria-describedby="passwordHelpBlock" required>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">Harap isi No Telepon.</div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form>
        </div>
    </div>

    <div class="content-body">
        <div class="container mb-5">
            <div class="card">
                <div class="card-body">
                <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Guru</p>
                <table id="myTable" class="table" style="padding: 27px;">
                    <thead>
                        <tr style="background-color: #DADADC; border-radius:30px; text-align:center;">
                    <tr>
                        <th>No.</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>No Telepon</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    @foreach ($guru as $g)
                    <tbody>
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $g->nip }}</td>
                        <td>{{ $g->nama }}</td>
                        <td>{{ $g->NoTelp }}</td>
                        <td> 
                            <button class="btn btn-info" data-toggle="modal" data-target="#perusahaan-edit-{{ $g->nip }}"><i class="fa-solid fa-pen"></i></button>
                            <a href="/hapus/guru/{{ $g->nip }}" class="btn btn-danger"><i class="fa-solid fa-trash"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <!-- Modal -->
                    @endforeach
                </table>
                </div>
                @foreach($guru as $g)
                <div class="modal fade" id="perusahaan-edit-{{ $g->nip }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        
                        <form action="/hubin/{{ $g->nip }}/edit_guru" method="POST">
                            @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Data {{  $g->nip }}</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <label for="inputPassword5" class="form-label">NIP</label>
                                <input type="number" value="{{ $g->nip }}" name="nip" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                                <label for="inputPassword5" class="form-label">Nama</label>
                                <input type="text" value="{{ $g->nama }}" name="nama" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                                <label for="inputPassword5" class="form-label">No Telepon</label>
                                <input type="number" value="{{ $g->NoTelp }}" name="NoTelp" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>   
</section>
@endsection