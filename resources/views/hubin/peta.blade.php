@extends('layouts.hubin.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Tambah
                  </button>
            </div>
        </div>
            <div class="row mt-4">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Id Pendaftaran</th>
                                <th scope="col">Id Periode</th>
                                <th scope="col">Nomor Perusahaan</th>
                                <th scope="col">Nis</th>
                                <th scope="col">Nip</th>
                                <th scope="col">Id Pembimbing</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                              <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $d->id_pendaftaran }}</td>
                                <td>{{ $d->id_periode }}</td>
                                <td>{{ $d->NoPerusahaan }}</td>
                                <td>{{ $d->Nis }}</td>
                                <td>{{ $d->nip }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="exampleFormControlInput1">Id Pendaftaran</label>
            <input type="text" name="id_pendaftaran" class="form-control" id="exampleFormControlInput1">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">Id Periode</label>
            <input type="text" name="id_periode" class="form-control" id="exampleFormControlInput1">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">Nomor Perusahaan</label>
            <input type="text" name="NoPerusahaan" class="form-control" id="exampleFormControlInput1">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">NIS</label>
            <div class="input-group">
              <select class="custom-select" id="inputGroupSelect04" aria-label="Example select with button addon">
                <option selected>Choose...</option>
                @foreach($pemetaan as $p)
                <option value="1">{{ $p->siswas->nis }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">NIP</label>
            <input type="text" name="nip" class="form-control" id="exampleFormControlInput1">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">Id Pembimbing</label>
            <input type="text" name="id_pembimbing" class="form-control" id="exampleFormControlInput1">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
    </div>
@endsection