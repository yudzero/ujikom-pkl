@extends('layouts.hubin.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <table class="table" id="myTable">
  <thead>
    <tr>
      <th scope="col">Nis</th>
      <th scope="col">Nama</th>
      <th scope="col">Kelas</th>
      <th scope="col">Tempat Lahir</th>
      <th scope="col">Alamat Siswa</th>
      <th scope="col">Nomor Telepon</th>
      <th scope="col">Aktivitas</th>
      <th scope="col">Keterangan</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $d)
    <tr>
      <th scope="row"><span class="badge bg-success">{{ $d->nis }}</span></th>
      <td>{{ $d->nama }}</td>
      <td>{{ $d->kelas }}</td>
      <td>{{ $d->Tmplahir }}</td>
      <td>{{ $d->Alamat_Siswa }}</td>
      <td>{{ $d->NoTelp }}</td>
      <td>{{ $d->keterangan }}</td>
      <td>{{ $d->created_at->diffForHumans(now()) }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
            </div>
        </div>
    </div>
@endsection