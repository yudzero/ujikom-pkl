<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
      crossorigin="anonymous"
    ></script>
    <title>Rekap pemetaan</title>
  </head>
  <style>
    table,
    th,
    tr {
      font-size: 13px;
    }
  </style>
  <body>
    <div class="container">
      <div style="text-align: center">
        <table>
          <tr>
            <td>
              <img
                style="width: 70px"
                src="http://p2k.unkris.ac.id/_sepakbola/_baca_image.php?td=9&kodegb=220px-West_Java_coa.jpg"
              />
            </td>
            <td>
              <p style="text-align: center; font-size: 14px">
                PEMERINTAH DAERAH PROVINSI JAWA BARAT<br />
                DINAS PENDIDIKAN <br />
                CABANG DINAS PENDIDIKAN WILAYAH VII<br />
                <span style="font-weight: 800; font-size: 16px"
                  >SMK NEGERI 11 KOTA BANDUNG<br
                /></span>
                <span style="font-size: 12px; font-weight: 400"
                  >Bisnis dan Manajemen - Teknologi Informasi dan
                  Komunikasi</span
                ><br />
                <span style="font-size: 13px; font-weight: 400"
                  >Jl/ Budhi Cilember (022) 6652442 Fax.(022) 6613508 Bandung
                  40175 <br />
                  http://smkn11bdg.sch.id E-mail:smkn11bdg@gmail.com
                </span>
              </p>
            </td>
          </tr>
        </table>
      </div>

      <table
        style="
          border: 1px solid black;
          text-align: center;
          border-collapse: collapse;
          margin-left: 12px;
          margin-top: 20px;
        "
      >
        <tr style="border: 1px solid black">
          <th style="padding: 12px; width:85px;">NIS</th>
          <th style="padding: 12px">Nama Siswa</th>
          <th style="padding: 12px">Nama Perusahaan</th>
          <th style="padding: 12px">Alamat Perusahaan</th>
          <th style="padding: 12px">Nama Pembimbing</th>
        </tr>
        @foreach($data as $d)
        <tr style="border: 1px solid black">
          <td>{{ $d->siswa->nis }}</td>
          <td>{{ $d->siswa->nama }}</td>
          <td>{{ $d->perusahaan->nama ?? 'Kosong' }}</td>
          <td>{{ $d->perusahaan->alamat }}</td>
          <td>{{ $d->guru->nama }}</td>
        </tr>
        @endforeach
      </table>
    </div>
  </body>
</html>
