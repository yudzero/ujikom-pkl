@extends('layouts.hubin.main')
@section('content')
<style>
  thead, th {text-align: center;}
.table thead,
.table th {text-align: center;}
#myTable thead,
#myTable th {text-align: center;}
.dataTables_length { display: none; }
</style>
    <section>
        
        <div class="container">
    @if ( Session::has('error'))
        <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
        </div>
    @endif
    </div>
{{-- crud --}}
    
        <div class="content-body">
            <div class="container mb-5">
                <div class="row">
                    <div class="col mb-3">
                        
                    <div class="modal fade" id="tambah-perusahaan" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form action="/tambah/siswa" method="POST" class="needs-validation" novalidate>
                            @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <label for="inputPassword5" class="form-label">Nis</label>
                                <input type="number"  name="nis" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                      Harap isi NIS.
                                    </div>
                                <label for="inputPassword5" class="form-label">Nama</label>
                                <input type="text"  name="nama" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                      Harap isi Nama.
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Jurusan</label>
                                        <select class="form-control needs-validation" name="id_jurusan" id="exampleFormControlSelect1">
                                            @foreach($jurusan as $j)
                                          <option value="{{ $j->id_jurusan }}">{{ $j->jurusan }}</option>
                                            @endforeach
                                        </select>
                                      </div>
                                <label for="inputPassword5" class="form-label">Kelas</label>
                                <input type="text"  name="kelas" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                      Harap isi Kelas.
                                    </div>
                                <label for="inputPassword5" class="form-label">Tanggal Lahir</label>
                                <input type="date" name="Tgllahir" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                      Harap isi Tanggal Lahir.
                                    </div>
                                <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                                <input type="text" name="Tmplahir" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                      Harap isi Tempat Lahir.
                                    </div>
                                <label for="inputPassword5" class="form-label">No Telepon</label>
                                <input type="text" name="NoTelp" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                      Harap isi No Telepon.
                                    </div>
                                <label for="inputPassword5" class="form-label">Alamat</label>
                                <input type="text" name="Alamat_Siswa" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                      Harap isi Alamat.
                                    </div>
                                <label for="inputPassword5" class="form-label">Email</label>
                                <input type="text" name="email" id="inputPassword5" class="form-control needs-validation"
                                    aria-describedby="passwordHelpBlock" required>
                                    <div id="validationServerEmailFeedback" class="invalid-feedback">
                                      Harap isi Email.
                                    </div>
                                    {{-- <label for="inputPassword5" class="form-label">Perusahaan</label>
                                <input type="text"  name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label">Jurusan</label>
                                <input type="text"  name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock"> --}}
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                <div class="card p-3">
                    <p class="mt-2 ml-5 mb-3" style="color:black; font-weight:700;">Daftar Murid</p>
                    <button class="btn btn-primary mb-2" data-toggle="modal" data-target="#tambah-perusahaan" style="width:15%; float: right;"> 
                            Tambah Data    
                        </button> 
                    <table id="myTable" class="table">
                        <thead>
                          <tr style="background-color: #DADADC; border-radius:30px; text-align:center;">
                            <tr>
                                <th>No</th>
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                {{-- <th>Email</th> --}}
                                <th>Status</th>
                                <th>Jurusan</th>
                                <th>Aksi</th>
                                {{-- <th>fhgjhkjl</th> --}}
                            </tr>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa as $s)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $s->nis }}</td>
                                <td>{{ $s->nama }}</td>
                                <td>{{ $s->kelas }}</td>
                                {{-- <td>{{ $s->email }}</td> --}}
                                <td>{{ $s->status ?? 'Belum Terdaftar!' }}</td>
                                <td>{{ $s->jurusan->jurusan ?? 'Kosong' }}  </td>
                                <td>
                                    <button class="btn btn-warning pl-2" data-toggle="modal"data-target="#logoutModal-edit-{{ $s->nis }}"><i class="fa-solid fa-pen"></i></button>
                                    {{-- <a href="" class="btn btn-info pl-2"><i class="fa-solid fa-eye"></i></a> --}}
                                    <a href="/hapus/siswa/{{ $s->nis }}" class="btn btn-danger pl-2"><i class="fa-solid fa-trash"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
                    </div>
                </div>

                    
                    <!--Modal-->
                    @foreach($siswa as $s)
                    <div class="modal fade" id="logoutModal-edit-{{ $s->nis }}" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            
                            <form action="/hubin/{{ $s->nis }}/edit_siswa" method="POST">
                                @csrf
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Data {{ $s->NamaSiswa }}</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="inputPassword5" class="form-label needs-validation" novalidate>NIS</label>
                                    <input type="nummber" value="{{ $s->nis }}" name="nis" id="inputPassword5" class="form-control"aria-describedby="passwordHelpBlock" disabled>
                                    <label for="inputPassword5" class="form-label needs-validation" novalidate>Nama Siswa</label>
                                    <input type="text" value="{{ $s->nama }}" name="nama" id="inputPassword5" class="form-control"aria-describedby="passwordHelpBlock">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1" class="needs-validation" novalidate>Jurusan</label>
                                            <select class="form-control" name="id_jurusan" id="exampleFormControlSelect1">
                                                @foreach($jurusan as $j)
                                              <option value="{{ $j->id_jurusan }}">{{ $j->jurusan }}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                    <label for="inputPassword5" class="form-label needs-validation" novalidate>Kelas</label>
                                    <input type="text" value="{{ $s->kelas }}" name="kelas" id="inputPassword5" class="form-control"aria-describedby="passwordHelpBlock">
                                        
                                    <label for="inputPassword5" class="form-label needs-validation" novalidate>Tanggal Lahir</label>
                                    <input type="date" value="{{ $s->Tgllahir }}" name="Tgllahir" id="inputPassword5" class="form-control"aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label needs-validation" novalidate>Tempat Lahir</label>
                                    <input type="text" value="{{ $s->Tmplahir }}" name="Tmplahir" id="inputPassword5" class="form-control"aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label needs-validation" novalidate>Alamat</label>
                                    <input type="text" value="{{ $s->Alamat_Siswa }}" name="Alamat_Siswa" id="inputPassword5" class="form-control"aria-describedby="passwordHelpBlock">
                                    {{-- <label for="inputPassword5" class="form-label needs-validation" novalidate>Nama Perusahaan</label> --}}
                                    {{-- <select class="form-control form-control-sm" name="">
                                        @foreach($perusahaan as $p)
                                        <option>{{ $p->NamaPerusahaan }}</option>
                                        @endforeach
                                      </select> --}}
                                    {{-- <label for="inputPassword5" class="form-label">Jurusan</label>
                                    <input type="text" value="{{ $s->jurusan->jurusan }}" name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock"> --}}
                                    
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
        
    </section>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.1.slim.js" integrity="sha256-tXm+sa1uzsbFnbXt8GJqsgi2Tw+m4BLGDof6eUPjbtk=" crossorigin="anonymous"></script>
        <script>
            $(document).ready( function () {
            $('#myTable').DataTable({
                "pageLength":5,
                "lengthMenu": [[5,10,20,-1],[5,10,20,"All"]],
            });
            });

        let tgllahir = document.querySelectorAll(".tgllahir");

        tgllahir.forEach((node)=>{
            let value = node.innerHTML;
            let date = new Date(value);
            let bulan = bulanF(date.getMonth());
            let last = value.split('-')
            
            node.innerHTML = `${last[2]} ${bulan} ${last[0]}`
        })

        function bulanF(number){
            let bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            return bulan[number];
        }

        
        </script>
@endsection
