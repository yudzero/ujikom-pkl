@extends('layouts.hubin.main')
@section('content')
    <div class="container">
        <table class="table" id="myTable">
            <thead>
              <tr>
                <th scope="col">NIS</th>
                <th scope="col">Nama</th>
                <th scope="col">Jurusan</th>
                <th scope="col">Kelas</th>
                <th scope="col">Email</th>
              </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < $array; $i++)
            <tr>
                <th scope="row"><span class="badge bg-success">{{ $testProc[$i]->nis }}</span></th>
                <td>{{ $testProc[$i]->nama }}</td>
                @foreach ($jurusan as $item)
                    @if ($testProc[$i]->id_jurusan == $item->id_jurusan)
                        <td>{{ $item->jurusan }}</td>
                    @endif
                @endforeach
                <td>{{ $testProc[$i]->kelas }}</td>
                <td>{{ $testProc[$i]->email }}</td>
            </tr>
            @endfor
              
              
            </tbody>
          </table>
    </div>
   
@endsection