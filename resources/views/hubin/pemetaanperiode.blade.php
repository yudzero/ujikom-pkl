@extends('layouts.hubin.main')
@section('content')
<style>
  thead, th {text-align: center;}
.table thead,
.table th {text-align: center;}
#myTable thead,
#myTable th {text-align: center;}
.dataTables_filter, .dataTables_info, .dataTables_length { display: none; }
</style>
<div class="container">
  <form action="{{ route('insertPeriode') }}" method="post">
    @csrf
    <div class="row">
      <div class="col-6">
    <label for="periode">Pilih Periode : </label>
    <select name="id_periode" id="periode" class="ml-4 pilih">
      <option selected disabled>Pilih periode dahulu</option>
      @foreach ($periode as $item)
        <option @if ($item->periode == $active)
            selected
        @endif value="{{ $item->id_periode }}" data-link="/hubin/periode?periode={{ $item->periode }}">{{ $item->periode }}</option>
      @endforeach
    </select>
    </div>
    </div>
    <div class="row">
        <table class="table mt-4"  id="myTable">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Perusahaan</th>
      <th scope="col">checklist</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($perusahaan as $p)
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $p->nama }}</td>
      <th><div class="form-check form-check-inline">
            <input class="form-check-input" name="checkbox[]" type="checkbox" id="checkBox1" value="{{ $p->NoPerusahaan }},">
            </div>
    </th>
    </tr>
    @endforeach
  </tbody>
</table>
    </div>
    <button class="btn btn-success mt-3" type="submit" style="float: left; width:90px" id="simpanBtn" disabled="disabled">save</button>
  </form>
</div>
<script>
  // var checkBox1 = document.getElementById('checkBox1');
  // var simpanBtn = document.getElementById('simpanBtn');
  var submit = document.getElementById('simpanBtn');
  var checkbox = document.querySelectorAll('.form-check-input');
  var disableSubmit = function(e) {
      submit.disabled = this.unchecked
  };

  checkbox.forEach(nopal => {
    nopal.disabled = true;
    nopal.addEventListener('change', disableSubmit);
  });

  document.getElementById("periode").addEventListener("change", function(){
    if (document.getElementById("checkBox1").disabled == true) {
      checkbox.forEach(element => {
        element.disabled = false;
      });
    }
  });


  
  // document.getElementById("pemetaanClose").addEventListener("click", function(){
  //   if (document.getElementById("rekapPemetaanBtn").disabled == false) {
  //     document.getElementById("rekapPemetaanBtn").disabled = true;
  //     document.getElementById('rekapPemetaanBtn').innerHTML = "Pilih Periode Dahulu";
  //   }
  // });
  // document.getElementById("simpanBtn").disabled = true;
  // document.getElementById("inlineCheckbox1").addEventListener("click", function(){
  //   if (document.getElementById("simpanBtn").disabled == true){
  //     document.getElementById("inlineCheckbox1").disabled == false;
  //   }
  // });
  // document.getElementById("inlineCheckbox1").addEventListener("change", function(){
  //   if (document.getElementById("simpanBtn").disabled == false){
  //     document.getElementById("simpanBtn").disabled == true;
  //   }
  // });

</script>
@endsection

{{-- @push('scriptBottom')
    <script>
      $(function () {
        $('#periode').change(function (e) { 
          window.open(e.target.options[e.target.selectedIndex].dataset.link, '_self');
        });
      })
    </script>
@endpush --}}