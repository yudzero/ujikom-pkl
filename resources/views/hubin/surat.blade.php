
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Surat Permohonan</title>

    <style>
        .page-break{
            page-break-after: always;
        }
    </style>
</head>
<style>
    .header{
        text-align: center;
    }
    .ttd{
        text-align: right;
    }
    .container{
        margin-top: 37px;
        margin-left: 94.5px;
        margin-bottom: 75.6px;
        margin-right: 75.6px;
    }
    *{
        font-size: 13px;
    }
</style>
<body>

    <div class="container">
    <div style="text-align: center;">
        <table>
            <tr>
                <td><img style="width: 70px;" src="http://p2k.unkris.ac.id/_sepakbola/_baca_image.php?td=9&kodegb=220px-West_Java_coa.jpg"></td>
                <td>
                <p style="text-align: center; font-size:14px;">PEMERINTAH DAERAH PROVINSI JAWA BARAT<br> DINAS PENDIDIKAN <br> CABANG DINAS PENDIDIKAN WILAYAH VII<br>
                <span style="font-weight:800;font-size:16px;">SMK NEGERI 11 KOTA BANDUNG<br></span>
                <span style="font-size: 12px; font-weight:400;">Bisnis dan Manajemen - Teknologi Informasi dan Komunikasi</span><br>
                <span style="font-size: 13px; font-weight:400;">Jl/ Budhi Cilember (022) 6652442 Fax.(022) 6613508 Bandung 40175 <br> http://smkn11bdg.sch.id E-mail:smkn11bdg@gmail.com </span></p>
                </td>
            </tr>
        </table>
        </div>
    <hr>
    <div class="content">
        <table>
          <tr>
            <td>No</td>
            <td>:</td>
            <td></td>
          </tr>
          <tr>
            <td>Hal</td>
            <td>:</td>
            <td>Permohonan Praktik Kerja Lapangan (PKL)</td>
          </tr>
          <tr>
            <td>Lamp</td>
            <td>:</td>
            <td>1 Bundel</td>
          </tr>
        </table>

        <p>Kepada<br>
         Yth Pimpinan {{ strtoupper($data->NamaPerusahaan); }}</p><!-- (nama pejabat) -->
        <p><!-- nama dunia kerja --></p>
        <p>
        <br>
        <p>Dengan Hormat,</p>
        <p>Dalam rangka pelaksanaan Pendidikan Vokasi terkait dengan program link and match guna meningkatkan kompetensi peserta didik, diwajibkan untuk
            melaksanakan Praktik Kerja Lapangan (PKL). Oleh karena itu kami mengajukan permohonan kepada Bapak/Ibu pimpinan 
            perusahaan agar dapat menerima peserta didik kami sebagai berikut (terlampir);
        </p><br>
        <p>Untuk melaksanakan PKL pada bagian/departemen yang ada di Perusahaan Bapak/Ibu. Adapun pelaksanaan PKL 
            kami rencanakan pada bulan <!-- bulan --> tahun <!-- tahun --> sampai dengan <!-- bulan --> tahun <!-- tahun -->
            atau sesuai dengan waktu yang Bapak/Ibu tentukan. Demikian surat permohonan ini kami ajukan, atas perhatiannya kami ucapkan 
            terima kasih.
        </p><br>
        <p class="ttd">
            Jakarta<!-- kota -->, 21 Januari 2023<!-- tanggal, bulan, tahun --> 
        </p>
        <br><br><br>
        <p class="ttd">Kepala SMK/MAK</p><!-- sekolah -->
    </div>
    </div>
    {{--  --}}

    <div class="page-break"></div>

     <div class="container">
    <div style="text-align: center;">
        <table>
            <tr>
                <td><img style="width: 70px;" src="http://p2k.unkris.ac.id/_sepakbola/_baca_image.php?td=9&kodegb=220px-West_Java_coa.jpg"></td>
                <td>
                <p style="text-align: center; font-size:14px;">PEMERINTAH DAERAH PROVINSI JAWA BARAT<br> DINAS PENDIDIKAN <br> CABANG DINAS PENDIDIKAN WILAYAH VII<br>
                <span style="font-weight:800;font-size:16px;">SMK NEGERI 11 KOTA BANDUNG<br></span>
                <span style="font-size: 12px; font-weight:400;">Bisnis dan Manajemen - Teknologi Informasi dan Komunikasi</span><br>
                <span style="font-size: 13px; font-weight:400;">Jl/ Budhi Cilember (022) 6652442 Fax.(022) 6613508 Bandung 40175 <br> http://smkn11bdg.sch.id E-mail:smkn11bdg@gmail.com </span></p>
                </td>
            </tr>
        </table>
        </div>
    <hr>
    <div class="content">
        <p style="text-align: center; font-size:16px;">DAFTAR NAMA SISWA YANG MENGAJUKAN PKL <BR> DI {{ strtoupper($data->NamaPerusahaan); }}<br>

            <table style="border: 1px solid black; text-align:center;border-collapse: collapse; margin-left:12px;margin-top:20px;">
                <tr style="border: 1px solid black;">
                    <th style="padding: 12px;">No</th>
                    <th style="padding: 12px;">Nomor Induk Siswa</th>
                    <th style="padding: 12px;">Nama Siswa</th>
                    <th style="padding: 12px;">P/L</th>
                    <th style="padding: 12px;">Kelas</th>
                    <th style="padding: 12px;">Program Keahlian</th>
                </tr>
                @foreach ($data->siswa as $d)
                <tr style="border: 1px solid black;">
                        <td style="border-bottom: 1px solid black;">{{ $loop->iteration }}</td>
                        <td style="padding: 12px;">{{ $d->nis }}</td>
                        <td style="padding: 12px;">{{ $d->nama }}</td>
                        <td style="padding: 12px;">L</td>
                        <td style="padding: 12px;">{{ $d->kelas }}</td>
                        <td style="padding: 12px;">{{ $d->jurusan->jurusan }}</td>
                    </tr>
                    @endforeach
            </table>
    </div>
</body>
</html>