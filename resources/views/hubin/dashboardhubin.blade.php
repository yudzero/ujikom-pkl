@extends('layouts.hubin.main')
@section('content')
<style>
  thead, th {text-align: center;}
.table thead,
.table th {text-align: center;}
#myTable thead,
#myTable th {text-align: center;}
.dataTables_filter, .dataTables_info, .dataTables_length { display: none; }
.dataTables_paginate{ padding: 20px;}

</style>

    <div class="container">
        <div class="row mt-5">

          <div class="col-3">
            <button type="button" class="btn " data-toggle="modal" data-target="#staticBackdrop">
            <div class="card">
              <div class="container">
                <div class="row">
                <div class="col-2">
                  <i class="fa-solid fa-print mr-2  ml-1 " style="font-size:40px; margin-top:32px;"></i>
                </div>
                <div class="col">
                  <p class="ml-4" style="text-align:left; font-size:16px; margin-top:30px;color:rgb(146, 146, 146); font-weight:800">Cetak Rekap Pemetaan</p>
                </div>
                </div>
            </div>
          </div>
         
            </button>
          </div>

          <div class="col-3">
             <button type="button" class="btn" data-toggle="modal" data-target="#staticBackdrop-perusahaan">
               <div class="card">
                 <div class="container">
                   <div class="row">
                   <div class="col-2">
                     <i class="fa-solid fa-print mr-2  ml-1 " style="font-size:40px; margin-top:32px;"></i>
                   </div>
                   <div class="col">
                     <p class="ml-4" style="text-align:left; font-size:16px; margin-top:30px;color:rgb(146, 146, 146); font-weight:800">Cetak Rekap Perusahaan</p>
                   </div>
                   </div>
               </div>
             </div>
            </button>
          </div>

          <div class="col-3">
            <a href="/hubin/historysiswa">
            <button type="button" class="btn">
               <div class="card">
                 <div class="container">
                   <div class="row">
                   <div class="col-2">
                     <i class="fa-solid fa-clock-rotate-left mr-2  ml-3" style="font-size:40px; margin-top:32px;margin-bottom:22px;"></i>
                   </div>
                   <div class="col">
                     <p class="ml-5" style="text-align:left; font-size:16px; margin-top:30px;color:rgb(146, 146, 146); font-weight:800">Lihat Histori</p>
                   </div>
                   </div>
               </div>
             </div>
            </button>
            </a>
          </div>

          <div class="col-3">
            <button type="button" class="btn">
               <div class="card">
                 <div class="container">
                   <div class="row">
                   <div class="col-2">
                    <i class="fa-solid fa-person" style="font-size: 40px;margin-top:32px;"></i>
                   </div>
                   <div class="col">
                    <a href="/hubin/siswadipetakan" style="text-decoration: none;">
                    <p class="ml-4" style="text-align:left; font-size:16px; margin-top:30px;color:rgb(146, 146, 146); font-weight:800">Siswa Terdaftar: <span style="text-align:left; font-size:16px; margin-top:30px;color:red; font-weight:800"> {{ $testFunc[0]->dataSiswa }}</span></p>
                    </a>
                    {{-- <p class="ml-3" style="text-align:left; font-size:16px; margin-top:30px;color:black; font-weight:800">{{ $testFunc[0]->dataSiswa }}</p> --}}
                  </div>
                   </div>
               </div>
             </div>
            </button>
          </div>
        </div>

<div class="row">
  <div class="col"></div>
    <div class="col-10">
        <div class="card mb-5 mt-4">
            <p class="mt-4 ml-5" style="color:black; font-weight:700;">Histori</p> 
            {{-- <p class="mt-4 ml-5" style="color:black; font-weight:700;">Fungsi Menghitung Siswa Diterima {{ $testFunc[0]->dataSiswa }}</p>  --}}
            <table class="table p-2" id="myTable">
              <thead>
                <tr>
                  <th scope="col">Nis</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Kelas</th>
                  <th scope="col">Aktivitas</th>
                  <th scope="col">Keterangan</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                <tr>
                  <th scope="row"><span class="badge bg-success">{{ $d->nis }}</span></th>
                  <td>{{ $d->nama }}</td>
                  <td>{{ $d->kelas }}</td>
                  <td>{{ $d->keterangan }}</td>
                  <td>{{ $d->created_at->diffForHumans() }}</td>
                    
                </tr>
                @endforeach
              </tbody>
            </table>
                </div>
            </div>
            <div class="col"></div>
                <!-- Button trigger modal -->

  {{-- <button type="button" class="btn btn-info mt-2" data-toggle="modal" data-target="#staticBackdrop"><i class="fa-solid fa-print mr-2"></i>Cetak Rekap Pemetaan</button><br> --}}


<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Cetak Rekap Pemetaan</h5>
      </div>
      <form action="{{ route('rekapPemetaan') }}">
      <div class="modal-body">
        
        <select name="id_periode" id="periodePemetaan" class="custom-select">
            <option selected disabled>Pilih Periode</option>
            @foreach($periode as $p)
            <option value="{{ $p->id_periode }}">{{ $p->periode }}</option>
            @endforeach
        </select>
        
      </div>
      <div class="modal-footer">
        <button type="button" id="pemetaanClose" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button id="rekapPemetaanBtn" type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
    </form>
  </div>
</div>

<!--Modal Rekap Perusahaan-->
{{-- <button type="button" class="btn btn-info mt-2" data-toggle="modal" data-target="#staticBackdrop-perusahaan"><i class="fa-solid fa-print mr-2"></i>Cetak Rekap Perusahaan</button><br> --}}


<!-- Modal -->
<div class="modal fade" id="staticBackdrop-perusahaan" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Cetak Rekap Perusahaan</h5>
      </div>
      <form action="{{ route('rekapPerusahaan') }}">
      <div class="modal-body">
        
        <select name="id_periode" id="periodePerusahaan" class="custom-select" required>
            <option selected disabled >Pilih Periode</option>
            @foreach($periode as $p)
            <option value="{{ $p->id_periode }}">{{ $p->periode }}</option>
            @endforeach
        </select>
        
      </div>
      <div class="modal-footer">
        <button type="button" id="perusahaanClose" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" id="rekapPerusahaanBtn" class="btn btn-primary">Kirim</button>
      </div>
    </div>
    </form>
  </div>
</div>
                
                {{-- <a href="{{ route('rekapPerusahaan') }}"><button type="button" class="btn btn-info mt-2"><i class="fa-solid fa-print mr-2"></i>Cetak Rekap Siswa</button></a> --}}
</div>
</div>
</div>
{{-- fungsi untuk disabled enable Button Rekap Pemetaan dan Rekap Perusahaan --}}
<script>

  document.getElementById("rekapPemetaanBtn").disabled = true;
  document.getElementById('rekapPemetaanBtn').innerHTML = "Pilih Periode Dahulu";
  document.getElementById("periodePemetaan").addEventListener("change", function(){
    if (document.getElementById("rekapPemetaanBtn").disabled == true) {
      document.getElementById("rekapPemetaanBtn").disabled = false;
      document.getElementById('rekapPemetaanBtn').innerHTML = "Cetak"
    }
  });
  document.getElementById("pemetaanClose").addEventListener("click", function(){
    if (document.getElementById("rekapPemetaanBtn").disabled == false) {
      document.getElementById("rekapPemetaanBtn").disabled = true;
      document.getElementById('rekapPemetaanBtn').innerHTML = "Pilih Periode Dahulu";
    }
  });

  
  document.getElementById("rekapPerusahaanBtn").disabled = true;
  document.getElementById('rekapPerusahaanBtn').innerHTML = "Pilih Periode Dahulu";
  document.getElementById("periodePerusahaan").addEventListener("change", function(){
    if (document.getElementById("rekapPerusahaanBtn").disabled == true) {
      document.getElementById("rekapPerusahaanBtn").disabled = false;
      document.getElementById('rekapPerusahaanBtn').innerHTML = "Cetak"
    }
  });
  document.getElementById("perusahaanClose").addEventListener("click", function(){
    if (document.getElementById("rekapPerusahaanBtn").disabled == false) {
      document.getElementById("rekapPerusahaanBtn").disabled = true;
      document.getElementById('rekapPerusahaanBtn').innerHTML = "Pilih Periode Dahulu";
    }
  });
  /* $(document).ready(function ()
  {
      $('#myTable').DataTable({
          "lengthMenu": [[25, 30, 50, -1], [25, 30, 50, "All"]],
          "responsive": true
      });
  }); */
</script>

@endsection