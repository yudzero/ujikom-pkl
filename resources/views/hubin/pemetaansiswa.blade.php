@extends('layouts.hubin.main')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-7">
    <label for="periode">Pilih Periode : </label>
    <select name="id_periode" id="periode" class="ml-4 pilih">
        <option selected disabled>Pilih Periode</option>
      @foreach ($periode as $item)
        <option @if ($item->periode == $active)
            selected
        @endif value="{{ $item->id_periode }}" data-link="/hubin/pemetaansiswa?periode={{ $item->periode }}">{{ $item->periode }}</option>
      @endforeach
    </select>
        </div>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    
    @foreach($perusahaan as $d)
    <div class="accordion2 ml-4 mt-4">
        <div class="row">
            <div class="col-7">
                <p class="ml-4 mt-4" style="font-weight: 600; font-size:18px;">{{ $d->nama }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <hr>
            </div>
        </div>

        

        @foreach($d->siswa as $item)
            <div class="row">
                <div class="col-8">
                    <p class=" ml-4">{{ $item->nama }}</p>
                </div>
                <div class="col-1 ml-3">
                    <form action="{{ route('terimaSiswa', $item->nis) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="nip" value="{{ $d->guru ? $d->guru->nip : '' }}">
                        <input type="hidden" name="id_periode" value="{{ $d->periode[0] ? $d->periode[0]->id_periode : '' }}">
                        {{-- @foreach ($data->periode as $item)
                            <input type="hidden" name="id_periode" value="{{ $item->id_periode }}">
                        @endforeach --}}
                        <button type="submit" style="text-decoration: none; border:none; background-color:white;"> <i class="fa-solid fa-circle-check" style="color: rgb(39, 220, 39);text-decoration: none; font-size:24px;"></i></button>
                    </div>
                </form>
                <div class="col-1">
                <form action="{{ route('tolakSiswa', $item->nis) }}" method="post">
                    @csrf
                    @method('PUT')
                    <button style="text-decoration: none; border:none; background-color:white;"><i class="fa-solid fa-ban" style="color: red; font-size:22px;"></i></button>
                </div>
                </form>
            </div>
        @endforeach
        
       </div>
        @endforeach
        
    

    <div class="row mt-4">
        <div class="col-8">

        </div>
        {{-- @foreach($app as $ap)
        {{ $ap->id }}
        @endforeach
        {!! $app->render() !!} --}}
        </div>
        
    </div>
    </div>
</div>
@endsection

@push('scriptBottom')
    <script>
      $(function () {
        $('#periode').change(function (e) { 
          window.open(e.target.options[e.target.selectedIndex].dataset.link, '_self');
        });
      })
    </script>
@endpush