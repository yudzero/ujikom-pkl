@extends('layouts.hubin.main')
@section('content')
<style>
  thead, th {text-align: center;}
.table thead,
.table th {text-align: center;}
#myTable thead,
#myTable th {text-align: center;}
.dataTables_filter, .dataTables_info, .dataTables_length { display: none; }
</style>
<div class="container">
    <div class="row">
        <div class="card mb-5">
            <p class="mt-4 ml-5" style="color:black; font-weight:700;">Perusahaan</p> 
            <div class="card-body">
                    <table id="myTable" class="table mb-5 mt-3">
                        <thead>
                        <tr>
                            <th>Nama Perusahaan</th>
                            <th>Alamat Perusahaan</th>
                            <th>Nomor FAX</th>
                            <th>Cetak Surat</th>
                        </tr>
                        </thead>
                        @foreach($perusahaan as $p)
                        <tbody>
                        <tr>
                            <td>{{ $p->nama }}</td>
                            <td>{{ $p->alamat }}</td>
                            <td>{{ $p->fax }}</td>
                            <td><a href="/cetakmurid/{{ $p->NoPerusahaan }}"><button class="buttonmaps">cetak</button></a></td>
                        </tr>
                        </tbody>
                        @endforeach
                    </table>
            </div>
                </div>
        </div>
    </div>
</div>
@endsection