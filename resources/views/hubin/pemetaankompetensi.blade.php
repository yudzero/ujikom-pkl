@extends('layouts.hubin.main')
@section('content')

    <div class="container">
      <div class="row">
        <div class="col-3"></div>
          <div class="col-6">
              <h4 style="text-align: center; font-weight:800;" class="mt-2 mb-4">Masukkan Data Kompetensi Keahlian</h4>
              <form action="/insertmateri" method="POST">
                @csrf
                <div class="mb-3 mt-5">
                  <label for="exampleInputEmail1" class="form-label">Elemen</label>
                  <select class="custom-select">
                    <option selected disabled>Open this select menu</option>
                    @foreach($data as $d)
                    <option value="{{ $d->id }}">{{ $d->element }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Materi Dasar</label>
                  <input type="text" name="NamaMateri" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Pelaksanaan Pembelajaran</label>
                  <select name="pelaksanaan_pembelajaran" class="form-control">
                    <option value="Di Sekolah">Di Sekolah</option>
                    <option value="Di Industri">Di Industri</option>
                  </select>
                </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Indikator Keberhasilan PKL</label>
                  <input type="text" name="indikator_keberhasilan" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Catatan</label>
                  <input type="text" name="catatan" class="form-control" id="exampleInputPassword1">
                </div>
                <button type="submit" class="btn btn-primary mt-3 mb-5" style="float: right; background-color:#395B64; border:transparent; width:90px;">Save</button>
              </form>
          </div>
          <div class="col-3"></div>
      </div>
  </div>
@endsection