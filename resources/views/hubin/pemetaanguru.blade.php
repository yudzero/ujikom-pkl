@extends('layouts.hubin.main')
@section('content')
<div class="body">
@foreach($data as $d)
<div class="box-pemetaan ml-5 mt-4 mb-4">
    <div class="row">
        <div class="col-7 mt-4 ml-4">
            <form action="/updateguru/{{ $d->NoPerusahaan }}" method="post">
        @csrf
        @method('put')
            <p style="font-weight: 600; font-size:18px;">{{ $d->nama }}</p>
            </div>
            
            <div class="col-2">
                <select class="mt-4  pilih-pembimbing" name="nip" id="nip" >
                    <option value="" disabled selected>Pilih Pembimbing</option>
                    @foreach($guru as $g)
                        <option @if ($d->nip == $g->nip) selected @endif value="{{ $g->nip }}">{{ $g->nama }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <button type="submit" class="btn btn-success mt-3 ml-3" style="height: 40px;">Save</button>
            </div>
            </div>
            </div>
    </form>
@endforeach
</div>
@endsection