@extends('layouts.hubin.main')
@section('content')

    <div class="container">
      <div class="row">
        <div class="col-3"></div>
          <div class="col-6">
              <h4 style="text-align: center; font-weight:800;" class="mt-2 mb-4">Masukkan Data Kompetensi Keahlian</h4>
              <form action="/insertelement" method="POST">
                @csrf
                <div class="mb-3 mt-5">
                  <label for="exampleInputEmail1" class="form-label">Elemen</label>
                  <input type="text" name="element" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3 mt-5">
                  <label for="exampleInputEmail1" class="form-label">Jurusan</label>
                 <select name="id_jurusan" class="custom-select">
                    <option selected disabled>Open this select menu</option>
                    @foreach($data as $d)
                    <option value="{{ $d->id_jurusan }}">{{ $d->jurusan }}</option>
                    @endforeach
                    </select>
                </div>
                <div class="mb-3 mt-5">
                  <label for="exampleInputEmail1" class="form-label">Tahun Ajar</label>
                  <input type="text" name="tahun_ajar" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                
                <button type="submit" class="btn btn-primary mt-3 mb-5" style="float: right; background-color:#395B64; border:transparent; width:90px;">Save</button>
              </form>
          </div>
          <div class="col-3"></div>
      </div>
  </div>
@endsection