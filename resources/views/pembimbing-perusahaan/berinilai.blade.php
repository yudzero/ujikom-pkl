@extends('layouts.pembimbingperusahaan.main')
@section('content')
    <div class="container">
        <h4 class="mt-5">Detail Data Siswa</h4>
        <div class="card mt-4" style="background-color: #EDEDED; border-radius: 20px; border: #EDEDED;">
            <div class="row mt-5 mb-5" style="margin: 0 auto; width: 100%;">
                <div class="col-1">
                </div>
                <div class="col" >
                    <p>Nama</p>
                    <p>Jurusan</p>
                    <p>Kelas</p>
                    <p>Tempat Tanggal Lahir</p>
                    <p>Alamat</p>
                </div>
                <div class="col-1">
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                </div>
                <div class="col">

                    <p>{{ $siswa->nama }}</p>
                    <p>{{ $siswa->jurusan->jurusan }}</p>
                    <p>{{ $siswa->kelas }}</p>
                    <p>{{ $siswa->Tmplahir }}, {{ $siswa->Tgllahir }}</p>
                    <p>{{ $siswa->Alamat_Siswa }}</p>

                </div>
            </div>
        </div>


        <div class="row mt-4 mb-3">
            <div style="font-weight: 800; text-align:center; font-size:20px;" class="col-4">
                Aspek Sikap
            </div>
            <div style="font-weight: 800; text-align:center; font-size:20px;" class="col-4">
                Aspek Pengetahuan
            </div>
            <div style="font-weight: 800; text-align:center; font-size:20px;" class="col-4">
                Aspek Keterampilan
            </div>
        </div>
        <form action="{{ route('postNilai') }}" method="post">
            @csrf
        <div class="row mt-4">
            <div class="col-4">
                <label>Penampilan Kerapihan Pakaian (A-E)</label>
                <input name="pkp"  class="form-control @error('pkp') is-invalid @enderror" value="{{ old('pkp') }}" autofocus>
                @error('pkp')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
             <div class="col-4">
                <label>Penguasaan Keilmuan (10-100)</label>
                <input type="number" class="form-control  @error('pk') is-invalid @enderror" value="{{ old('pk') }}" name="pk" >
                @error('pk')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
             <div class="col-4">
                <label>Keahlian dan Keterampilan (10-100)</label>
                <input type="number" class="form-control  @error('kk') is-invalid @enderror" value="{{ old('kk') }}" name="kk" >
                @error('kk')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <label>Komitmen dan Integritas  (A-E)</label>
                <input type="text" class="form-control @error('ki') is-invalid @enderror" value="{{ old('ki') }}" name="ki" >
                @error('ki')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
             <div class="col-4">
                <label>Kemampuan Mengidentifikasi Masalah (10-100)</label>
                <input type="number" class="form-control @error('kmm') is-invalid @enderror" value="{{ old('kmm') }}" name="kmm" >
                 @error('kmm')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
             <div class="col-4">
                <label>Inovasi dan Kreativitas (10-100)</label>
                <input type="number" class="form-control @error('ik') is-invalid @enderror" value="{{ old('ik') }}" name="ik" >
                 @error('ik')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <label>Menghargai dan Menghormati  (A-E)</label>
                <input type="text" class="form-control @error('mm') is-invalid @enderror" value="{{ old('mm') }}" name="mm" >
                @error('mm')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
             <div class="col-4">
                <label style="font-size: 16px;">Kemampuan Menemukan Alternatif Solusi Secara Kreatif (10-100)</label>
                <input type="number" class="form-control @error('kmassk') is-invalid @enderror" value="{{ old('kmassk') }}" name="kmassk" >
                 @error('kmassk')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
             <div class="col-4">
                <label>Produktivitas dan Penyelesaian Tugas (10-100)</label>
                <input type="number" min="10" class="form-control @error('ppt') is-invalid @enderror" value="{{ old('ppt') }}" name="ppt" >
                 @error('ppt')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            
        </div>

        <div class="row ">
            <div class="col-4">
                <label>Kreativitas (A-E)</label>
                <input type="text" pattern="(A|B|C|D|E)" class="form-control @error('kreativitas') is-invalid @enderror" value="{{ old('kreativitas') }}" name="kreativitas" >
                @error('kreativitas')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-4">
                
            </div>
            <div class="col-4">
                <label>Penguasaan Alat Kerja (10-100)</label>
                <input type="number" class="form-control @error('pak') is-invalid @enderror" value="{{ old('pak') }}" name="pak" >
                 @error('pak')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>  
       
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <label>Kerjasama Tim (A-E)</label>
                <input type="text" class="form-control @error('kt') is-invalid @enderror" value="{{ old('kt') }}" name="kt" >
                @error('kt')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-4">

            </div> 
        </div>
       

        <div class="row mt-2">
            <div class="col-4">
                <label>Disiplin dan Tanggung jawab (A-E)</label>
                <input type="text" class="form-control @error('dt') is-invalid @enderror" value="{{ old('dt') }}" name="dt" >
                @error('dt')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-4">
                <input type="text" class="form-control" value="{{ $siswa->nis }}" name="nis" hidden>
            </div>
        </div>

        

        <div class="row mt-2 mb-4">
            <div class="col-4">
                <button class="btn btn-success" type="submit">Save</button>
            </div>
        </div>
    </form>
    </div>

@endsection