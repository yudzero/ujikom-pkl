@extends('layouts.pembimbingperusahaan.main')
@section('content')


    <section>
        <div class="content-body">
            <div class="container mb-5">
                <div class="card">
                    <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Murid</p>
                      <div class="card-body">
                      <table id="myTable" class="table">
                        <thead>
                          <tr style="background-color: #DADADC; border-radius:30px;">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Nama Perusahaan</th>
                                @if (empty($s->nilaisiswa))
                                <th>Beri Nilai</th>
                                @else
                                    <th>Edit Nilai</th>
                                @endif
                                
                            </tr>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa->siswa as $s)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $s->nama }}</td>
                                <td>{{ $s->kelas }}</td>
                                <td>{{ $s->perusahaan->nama ?? 'Belum Terdaftar' }}</td>
                             
                                @if (empty($s->nilaisiswa))
                                    <td><a href="/perusahaan/berinilai/{{ $s->nis }}" class="btn btn-primary btn-circle"><i class="fa-solid fa-pencil"></i></a></td>
                                @else
                                    <td><a href="/perusahaan/edit/{{ $s->nis }}" class="btn btn-warning btn-circle"><i class="fa-solid fa-pencil"></i></a></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </section>

@endsection