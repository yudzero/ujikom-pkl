@extends('layouts.pembimbingperusahaan.main')
@section('content')
<style>
    .card{
        height: 135px;
        width: 300px;
    }
</style>
    <div class="container">
        <h2 class="mt-4" style="color:black; font-weight:600;"> Hallo, {{ $perusahaan->nama }}</h2>
        <p class="mt-3" style="width: 70%;color:black">Selamat datang di E-Prakerin, website yang dibuat khusus untuk memudahkan perusahaan Anda dalam mengelola data nilai siswa</p>
        
        <div class="row">
            <div class="col-2"></div>
            <div class="col-4">
                <a href="/perusahaan/daftarsiswa">
                    <div class="card mt-4" style="text-align: center">
                        <i class="fa-solid fa-person" style="font-size: 39px; margin-top:35px;"></i>
                        <p>Lihat Siswa</p>
                    </div>
                </a>
            </div>
            <div class="col-4">
                <a href="/perusahaan/daftarsiswa">
                    <div class="card mt-4" style="text-align: center">
                        <i class="fa-solid fa-percent" style="font-size: 39px; margin-top:35px;"></i>
                        <p>Beri Nilai</p>
                    </div>
                </a>
            </div>
        </div>

        {{-- <div class="row">
            <div class="col-2"></div>
            <div class="col-4">
                <a href="perusahaan/daftarsiswa">
                    <div class="card mt-4" style="text-align: center">
                        <i class="fa-solid fa-screwdriver-wrench" style="font-size: 39px; margin-top:35px;"></i>
                        <p>Kompetensi Keahlian</p>
                    </div>
                </a>
            </div>
            <div class="col-4">
                <a href="perusahaan/daftarsiswa">
                    <div class="card mt-4" style="text-align: center">
                        <i class="fa-solid fa-book" style="font-size: 39px; margin-top:35px;"></i>
                        <p>Aktivitas Siswa</p>
                    </div>
                </a>
            </div> --}}

        </div>

    </div>  
@endsection
