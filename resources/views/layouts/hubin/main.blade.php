<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $title }}</title>
    

    <!-- Custom fonts for this template-->
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;500;600&display=swap" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    
</head>

<style>
    .pilih{
        border-radius: 10px;
        font-size: 14px;
        height: 30px;
        2width: 100px;
    }
    .buttonmaps:hover{
        background-color: #b3b3b3;
    }
    .pilih-pembimbing{
        float: right;
        text-align: center;
        height: 32px;
        width:190px;
        border-radius: 10px;
    }
    .box-pemetaan{
        background-color: white;
        width: 90%;
        height: 70px;
        border-radius: 20px;
        border: transparent; 
        text-align: left;
        box-shadow: 1px 0px 10px rgb(206, 206, 206);
    }
    input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

    .accordion2{
        background-color: white;
        width: 1030px;
        height: auto;
        border-radius: 20px;
        transition: 0.4s;
        border: transparent; 
        text-align: left;
        box-shadow: 1px 0px 10px rgb(206, 206, 206);
    }
    .accordion1 {
      background-color: transparent;
      color: #444;
      cursor: pointer;
      padding: 18px;
      width: 100%;
      border: none;
      text-align: left;
      outline: none;
      font-size: 15px;
      transition: 0.4s;
    }
    .active .accordion2:hover {
        background-color: white;
    }
    .active, .accordion1:hover,{
      background-color: transparent;
    }
    .panel1 {
        padding: 0 18px;
        width: 1030px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
        border-radius: 20px;
        box-shadow: 1px 0px 10px rgb(206, 206, 206);
    }
    
    .panel {
      background-color: transparent;
      max-height: 0;
      font-size: 12px;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
    }
    .panel2{
        text-decoration: none;
        color: rgba(255, 255, 255, 0.8);
        font-size: 9px;
    }
    .nav-item{
        margin-bottom:-10px;
    }
    span{
        text-align: center;
    }

    /* CSS Splash Screen */
    .scontainer{

        width: 100vw; 
        height: 100vh;
        position: fixed;
        background-color:#42575c;
        z-index: 999;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .sk-cube-grid {
        width: 40px;
        height: 40px;
        margin: 100px auto;
    }

.sk-cube-grid .sk-cube {
  width: 33%;
  height: 33%;
  background-color: white;
  float: left;
  -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
          animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out; 
}
.sk-cube-grid .sk-cube1 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube2 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube3 {
  -webkit-animation-delay: 0.4s;
          animation-delay: 0.4s; }
.sk-cube-grid .sk-cube4 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube5 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube6 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube7 {
  -webkit-animation-delay: 0s;
          animation-delay: 0s; }
.sk-cube-grid .sk-cube8 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube9 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }

@-webkit-keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1); 
  }
}

@keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1);
  } 
}
    </style>

    

<body id="page-top">

    {{-- Div Splash Screen --}}
    <div class="scontainer">
        <div class="sk-cube-grid" id="splashScreen">
          <div class="sk-cube sk-cube1"></div>
          <div class="sk-cube sk-cube2"></div>
          <div class="sk-cube sk-cube3"></div>
          <div class="sk-cube sk-cube4"></div>
          <div class="sk-cube sk-cube5"></div>
          <div class="sk-cube sk-cube6"></div>
          <div class="sk-cube sk-cube7"></div>
          <div class="sk-cube sk-cube8"></div>
          <div class="sk-cube sk-cube9"></div>
        </div>
    </div>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: #395B64;">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/dashboard/hubin') }}">
                <div class="sidebar-brand-icon">
                    <img src="{{ asset('assets/img/e-prakerin white.png') }}" alt="" style="width: 100%;">
                </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item list">
                <a class="nav-link" href="{{ url('/dashboard/hubin') }}">
                    <i class="fa-solid fa-grip"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item list">
                <a class="nav-link collapsed" href="{{ url('/hubin/jurusan') }}" 
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fa-solid fa-school"></i>
                    <span >Jurusan</span>
                </a>
            </li>
            
            <li class="nav-item list">
                <a class="nav-link collapsed" href="{{ url('/hubin/siswa') }}" 
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fa-solid fa-person"></i>
                    <span >Siswa</span>
                </a>
            </li>
            
            <li class="nav-item list">
                <a class="nav-link collapsed" href="{{ url('/hubin/guru') }}" 
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fa-solid fa-chalkboard-user"></i>
                    <span >Guru</span>
                </a>
            </li>


            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item list">
                <a class="nav-link collapsed" href="{{ url('/hubin/perusahaan') }}" 
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fa-solid fa-industry"></i>
                    <span >Perusahaan</span>
                </a>
            </li>
            
            
            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item list">
                <button class="accordion1" style="padding-top:0px;">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                        <i class="fa-solid fa-graduation-cap"></i>
                        <span style="margin-left: -20%;">Pemetaan</span>
                    </a>
                </button>
                <div class="panel" style="text-align: center; margin-top:-25px;">
                    <a class="collapse-item panel2" href="{{ url('/hubin/periode') }}">Pemetaan Periode</a><hr style="background-color:white; margin-top:10px; margin-bottom:10px; width:100%; ">
                    <a class="collapse-item panel2" href="{{ url('/hubin/pemetaanguru') }}">Pemetaan Guru</a><hr style="background-color:white; margin-top:10px; margin-bottom:10px; width:100%; ">
                    <a class="collapse-item panel2" href="{{ url('/hubin/pemetaansiswa') }}">Pemetaan Siswa</a><hr style="background-color:white; margin-top:10px; margin-bottom:10px; width:100%; ">
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item list">
                <a class="nav-link collapsed" href="{{ url('/hubin/cetaksurat') }}" 
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fa-solid fa-envelope"></i>
                    <span >Cetak Surat</span>
                </a>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item list">
                <button class="accordion1" style="margin-top: -12px;">
                <a class="nav-link collapsed"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fa-solid fa-file-export"></i>
                    <span>Import Data</span>
                </a>
                </button>
                <div class="panel" style="text-align: center; margin-top:-25px;">
                    <a class="collapse-item panel2" href="{{ url('/hubin/importdata') }}">Import User Data</a><hr style="background-color:white; margin-top:10px; margin-bottom:10px; width:100%; ">
                    <a class="collapse-item panel2" href="{{ url('/hubin/importsiswa') }}">Import Data Siswa</a><hr style="background-color:white; margin-top:10px; margin-bottom:10px; width:100%; ">
                    <a class="collapse-item panel2" href="{{ url('/hubin/importguru') }}">Import Data Guru</a>
                </div>
            </li>

            
            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item list">
                <a class="nav-link collapsed" href="http://localhost/ujikom-prakerin/public/hubin/backup" 
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fa-solid fa-envelope"></i>
                    <span >Backup database</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider mt-4">

            <!-- Heading -->
        
            <!-- Sidebar Toggler (Sidebar) -->

            <!-- Sidebar Message -->
       

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!--Header-->
                    <div class="row ml-2">
                        <div class="col">
                            <h3>{{ $titleheader }}</h3>
                        </div>
                    </div>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                       


                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Hubin</span>
                                <img class="img-profile rounded-circle"
                                    src="{{ asset('assets/img/undraw_profile.svg') }}">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="/logout" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->
                @yield('content')

            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Pilih "Logout" di bawah jika Anda siap untuk mengakhiri sesi Anda saat ini.<div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="{{ route('logout') }}">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        /* Script SplashScreen */
        window.onload = function(){
            setTimeout(() => {
                document.getElementsByClassName("scontainer")[0].style.display = "none";
            }, 1000);
        }




        
        var acc = document.getElementsByClassName("accordion1");
        var i;
        
        for (i = 0; i < acc.length; i++) {
          acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            } 
          });
        }
        </script>
       

    <script src="{{ asset('assets/js/script.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <!--Highchart-->
    <script src="https://code.highcharts.com/highcharts.js"></script>


    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('assets/vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('assets/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('assets/js/demo/chart-pie-demo.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
          'use strict'
        
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.querySelectorAll('.needs-validation')
        
          // Loop over them and prevent submission
          Array.prototype.slice.call(forms)
            .forEach(function (form) {
              form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                  event.preventDefault()
                  event.stopPropagation()
                }
            
                form.classList.add('was-validated')
              }, false)
            })
        })()
        </script>

    <script type="text/javascript">
        $(document).ready( function () {
    $('#myTable').DataTable();
    } );
    </script>

    @include('sweetalert::alert')

    @stack('scriptBottom')
</body>

</html>