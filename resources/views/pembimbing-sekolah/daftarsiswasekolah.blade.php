@extends('layouts.pembimbingsekolah.main')
@section('content')
    <div class="container">
        <div class="row">
        </div>
        <section>
            <div class="content-body">
                <div class="container mb-5">
                    <div class="card">
                        <p class="mt-4 ml-5" style="color:black; font-weight:700;">Semua Murid</p> 
                        <div class="dropdown">
                            <div class="row">
                                <div class="col-8">
                                </div>
                            {{-- <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                              Periode
                            </button>
                            <div class="dropdown-menu">
                              <button class="dropdown-item" type="button">Januari-Juni</button>
                              <button class="dropdown-item" type="button">Juni-Desember</button>
                            </div>
                          <div class="dropdown">
                            <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                Tahun
                            </button>
                            <div class="dropdown-menu">
                              <button class="dropdown-item" type="button">2022</button>
                              <button class="dropdown-item" type="button">2023</button>
                              <button class="dropdown-item" type="button">2024</button>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                              Divisi
                            </button>
                            <div class="dropdown-menu">
                              <button class="dropdown-item" type="button">IT</button>
                              <button class="dropdown-item" type="button">Marketing</button>
                            </div>
                        </div> --}}
                          </div>
                        <table class="tabelperusahaan mb-5 mt-3" id="myTable">
                          <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jurusan</th>
                                <th>Kelas</th>
                                <th>Nama Perusahaan</th>
                                <th>Aksi</th>
                            </tr>
                          </thead>
                            @foreach ($data as $d)
                            <tbody>
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $d->siswa->nama }}</td>
                                <td>{{ $d->siswa->jurusan->jurusan }}</td>
                                <td>{{ $d->siswa->kelas }}</td>
                                <td>{{ $d->perusahaan->nama ?? 'Kosong'}}</td>
                                <td><a href="/siswa/sikap/{{ $d->nis }}" class="btn btn-circle btn-primary"><i class="fa-solid fa-magnifying-glass"></i></a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection