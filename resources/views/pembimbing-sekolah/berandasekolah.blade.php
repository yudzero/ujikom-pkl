@extends('layouts.pembimbingsekolah.main')
@section('content')
<style>
</style>
<div class="container">
    <div class="row">
        <div class="col-6">
            <h3 class="mt-3" style="color: black; font-weight:600;">Hello, {{ $guru->nama}}</h3>
            {{-- <div class="dropdown">
              <button style="background-color: #577386;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                Pilih Perusahaan
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item" href="#">Scola LMS</a></li>
                <li><a class="dropdown-item" href="#">Chlorine Digital Media</a></li>
                <li><a class="dropdown-item" href="#">FS Indonesia</a></li>
              </ul>
            </div> --}}
        </div>
        <div class="col-4">
            <div class="card mt-3" style="box-shadow: 0px 0px 12px 1px #3e3e3e32;">
                <a href="" style="text-decoration: none; color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Jumlah Siswa</h5>
                    <h1 class="mb-4" style="text-align: center;">{{ $siswa->count() }}</h1>
                    </a>
            </div>
        </div>
        {{-- <div class="col-4">
            <div class="card mt-3" style="box-shadow: 0px 0px 12px 1px #3e3e3e32;">
                <a href="" style="text-decoration: none; color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Jumlah Perusahaan</h5>
                    <h1 class="mb-4" style="text-align: center;">2</h1>
                    </a>
            </div>
        </div> --}}
        <div class="row mt-5">
            <div class="col">
                <div class="card mb-5">
                    <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Murid</p> 
                    <table id="myTable" class="table ml-2" style="width:97%;">
                       <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Nama Perusahaan</th>
                        </tr>
                       </thead>
                    @foreach($siswa as $s)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $s->siswa->nama }}</td>
                            <td>{{ $s->siswa->kelas }}</td>
                            <td>{{ $s->perusahaan->nama ?? 'Belum Terdaftar' }}</td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>

        </div>

        <div class="row">
            {{-- <div class="col-3 mt-5">
                <div class="card" style="color: black;box-shadow: 0px 0px 12px 1px #3e3e3e32;">
                    <h5 class="card-title mt-4" style="text-align: center;">Lihat Evaluasi Prakerin</h5>
                    <p style="text-align: center;">PT.Maju Mundur</p>
                    <a style="text-align: center;" class=" mt-5 mb-4" href=""><i style="font-size: 60px;" class="fa-solid fa-circle-arrow-right mb-5"></i></a>
                </div>
            </div> --}}
            <div class="col-9">
                <div class="tw-bg-white tw-shadow-md tw-h-fit tw-py-10 tw-w-full mt-5" style="width: 500px; margin:0 auto;">
                   <div class="tw-px-10 tw-font-pop mt-5">
                     <div>
                       {{-- <canvas id="myChart" class="tw-mt-5"></canvas>           --}}
                     </div>          
                   </div>
                 </div>
            </div>
        </div>
    </div>
    <div>
  {{-- <div id="myChart"></div> --}}
  <div id="chart2"></div>
</div>
    {{-- <canvas id="userChart" class="rounded shadow" style="width:100%; max-width:500px; height:100%; max-height:500px;"></canvas> --}}
</div>
<!--CDN Chartjs-->
    {{-- <script src="{{ asset('node_modules/chart.js/dist/chart.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script> --}}


<script src={{ asset('js/highcharts.js') }}></script>
<script>
    Highcharts.chart('myChart', {

    title: {
        text: 'Diagram Absensi Kehadiran Siswa',
        align: 'left'
    },

    subtitle: {
        text: 'Source: <a href="https://irecusa.org/programs/solar-jobs-census/" target="_blank">IREC</a>',
        align: 'left'
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },

    xAxis: {
        accessibility: {
            rangeDescription: 'Range: 2010 to 2020'
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2023
        }
    },

    series: [{
        name: [
            @foreach ($chart as $c)
            '{{ $c->keterangan }}',
            @endforeach
        ],
        data: [
            @foreach ($chart as $chart)
            {{ $chart->jumlah }},
            @endforeach
        ]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
</script>
<script>
    // Data retrieved from https://netmarketshare.com
Highcharts.chart('chart2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Diagram Kehadiran Siswa',
        align: 'left'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Hadir',
            y: parseInt("{{ $chartData['hadir'] }}"),
            sliced: true,
            selected: true
        }, {
            name: 'Izin',
            y: parseInt("{{ $chartData['izin'] }}") ,
        },  {
            name: 'Sakit',
            y: parseInt("{{ $chartData['sakit'] }}"),
        }, {
            name: 'Alpha',
            y: parseInt("{{ $chartData['alpha'] }}"),
        }]
    }]
});
</script>
{{-- <script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: {!! json_encode($groups->pluck('tanggal')) !!},
            datasets: [{
                label: 'Kehadiran',
                data: {!! json_encode($groups->pluck('jumlah_kehadiran')) !!},
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script> --}}




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@endsection