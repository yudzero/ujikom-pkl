@extends('layouts.siswa.main')
@section('content')
<style>
.dataTables_filter, .dataTables_info, .dataTables_length { display: none; }
  thead, th {text-align: center;}
.table thead,
.table th {text-align: center;}
#myTable thead,
#myTable th {text-align: center;}
</style>
    <div class="container">
        <div class="row">
            <table id="myTable" class="table">
              <thead>
                <tr style="background-color: #DADADC; border-radius:30px;">
                  <th scope="col">Nama Perusahaan</th>
                  <th scope="col">Jurusan yang dibutuhkan</th>
                  <th scope="col">Map</th>
                  @if($siswas->pemetaan)
                        @if($siswas->pemetaan->status == 'pending')
                        <th>Status</th>
                        @elseif($siswas->pemetaan->status == 'diterima')
                        <th>Status</th>
                        @endif
                    @else
                    <th scope="col">Action</th>
                  @endif
                  
                </tr>
              </thead>
              <tbody>
                @foreach($perusahaan as $p)
                <tr>
                  <td>{{ $p->nama }}</td>
                  <td>  <button type="button" class="buttonmaps" data-toggle="modal" data-target="#exampleModal-detail-{{ $p->NoPerusahaan }}">
                          Lihat Detail
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal-detail-{{ $p->NoPerusahaan }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Detail Perusahaan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ $p->DeskripsiPerusahaan }}
                               <br><br>
                                Jurusan yang dibutuhkan : {{ $p->jurusan }}
                                <br><button type="button" class="btn btn-secondary" style="float: right" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </td>
                    <td><a href="{{ $p->maps }}">
                        <button class="buttonmaps">maps <i class="fa-solid fa-arrow-right ml-1"></i></button></a> 
                    </td>
                    @if ($siswas->pemetaan)
                        @if ($siswas->pemetaan->status == 'pending')
                            <td>Permintaan anda sedang diproses di <br> {{ $siswas->pemetaan->perusahaan->nama }}</td>
                        @elseif($siswas->pemetaan->status == 'diterima')
                            <td>Anda Telah Diterima di<br> {{ $siswas->pemetaan->perusahaan->nama }}, Silahkan hubungi Hubin!</td>
                        @endif
                    @else
                        <td><a class="dropdown-item" href="#" data-toggle="modal" data-target="#daftarmodal-{{ $p->NoPerusahaan }}">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Daftar
                            </a>    
                        </td>
                    @endif
                </tr>
                @endforeach
              </tbody>
            </table>
            @foreach($perusahaan as $p)
            <div class="modal fade" id="daftarmodal-{{ $p->NoPerusahaan }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="/siswadaftar" method="POST">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Form Pendaftaran PKL</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">X</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               
                                <label for="disabledTextInput" class="form-label">Nis</label>
                                <input type="text" value="{{ $siswas->nis }}" name="nis" hidden>
                                <input type="text" value="{{ $siswas->nis }}" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" disabled>

                                <input type="text" value="{{ $p->NoPerusahaan }}" name="NoPerusahaan" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" style="display: none;">
                                
                                {{-- <label for="inputPassword5" class="form-label">Periode</label>
                                <input type="text" value="{{ Auth::user()->siswa->nis }}" name="id_periode" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock"> --}}
                               
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-primary" type="submit">Daftar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection