@extends('layouts.siswa.main')
@section('content')
<div class="container" style="margin-left: 30%">
  <div class="row">
    Isi Keterangan :
  </div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="sakit" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
    Sakit
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="izin" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
    Izin
  </label>
</div>

<br>
<div class="row">
  <form>
  <div class="form-group">
    <label for="formGroupExampleInput">Alasan tidak hadir :</label>
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Masukkan keterangan" style="width: 150%;">
  </div>
</form>
</div>

<div class="row mt-5" >
  <div class="col-5">
  <button class="btn btn-success mr-2 ml-5">Simpan</button>
  <a href="/dashboard/siswa"><button class="btn btn-secondary">Kembali</button></a>
</div>
</div>
</div>
@endsection