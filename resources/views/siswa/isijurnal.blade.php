@extends('layouts.siswa.main')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Isi Jurnal</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;500;600&display=swap" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
    label{
        font-weight: 800;
        color: black;
    }
    /* :root {
  --background-gradient: linear-gradient(30deg, #f39c12 30%, #f1c40f);
  --gray: #34495e;
  --darkgray: #213d3a;
} */

selectbox {
  /* Reset Select */
  appearance: none;
  outline: 0;
  border: 0;
  box-shadow: none;
  /* Personalize */
  flex: 1;
  padding: 0 1em;
  color: #ffffff;
  background-color: var(--darkgray);
  background-image: none;
  cursor: pointer;
}
/* Remove IE arrow */
select::-ms-expand {
  display: none;
}
/* Custom Select wrapper */
.selectbox {
  position: relative;
  display: flex;
  width: 14em;
  height: 2em;
  border-radius: .25em;
  overflow: hidden;
}
/* Arrow */
.select::after {
  content: '\25BC';
  position: absolute;
  top: 0;
  right: 0;
  padding: 1em;
  background-color: #626569;
  transition: .25s all ease;
  pointer-events: none;
}
/* Transition */
.select:hover::after {
  color: #f39c12;
}

/* Other styles*/
/* body {
  color: #fff;
  background: var(--background-gradient);
} */
/* h1 {
  margin: 0 0 0.25em;
}
a {
  font-weight: bold;
  color: var(--gray);
  text-decoration: none;
  padding: .25em;
  border-radius: .25em;
  background: white;
} */

    </style>
<body>
    <div class="container">
{{-- Batas Notifikasi --}}
        <h3 class="mt-5" style="text-align: center; font-weight:800; color:black;">Pengisian Jurnal Prakerin</h3>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8 mt-5">
        @if ($check)
        
        <div class="card ml-5" style="height:250px; width:600px;">
          <div class="row">
            <div class="col"></div>
            <div class="col">
              <img style="width:45%;" class="ml-5 mt-5" src="{{ asset('assets/img/checklist.gif') }}">
            </div>
            <div class="col"></div>
          </div>
          <h3 style="text-align: center; font-weight:600;" class="mt-5">Anda sudah absen!</h3>
        </div>

        @else
          <form action="{{ route('insertJurnal') }}" method="POST">
            @csrf
            <div class="container">
            <div class="row">
                <div class="col-4">
                  <label class="form-label">Pilih Kehadiran :</label>
                  <select class="selectbox" name="keterangan" id="kehadiran" onchange = "EnableDisableTextBox(this)">
                    <option selected disabled>Pilih salah satu</option>
                    <option value="hadir">Hadir</option>
                    <option value="berhalangan">Berhalangan</option>
                  </select>
                </div>
                <div class="col"></div>
                <div class="col-4">
                  <label class="form-label">Pilih Alasan :</label>
                  <select class="selectbox" name="keterangan1" id="alasan" disabled="true" />
                    <option selected disabled>Pilih salah satu</option>
                    <option value="SAKIT">Sakit</option>
                    <option value="IZIN">Izin</option>
                  </select>
                </div>
            </div>
                <div class="row">
                  <div class="col mt-4">
                      <label for="exampleFormControlInput1" class="form-label" for="aktivitas">Aktivitas PKL</label>
                    <input type="text" name="aktivitas" class="form-control @error('aktivitas') is-invalid @enderror" id="isiKehadiran" disabled="disabled" />
                    @error('aktivitas')
                        <div class="alert alert-danger">
                          {{ $message }}
                        </div>
                    @enderror
                  </div>
                    </div>
                  </div>
                </div>
              </div>
    
              {{-- <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Hari/Tanggal Pelaksanaan</label>
                <input type="date" name="tanggal" class="form-control"  id="exampleFormControlInput1">
              </div> --}}
              <input type="hidden" name="nis" value="{{ $siswas->nis }}">
          
              <button class="btn btn-primary mt-5 mb-5 " style="float: right;margin-right:150px;background-color:#395B64; border:transparent; width:130px; text-align:center;">Submit</button>
          </form>
        @endif
            </div>
            <div class="col-2"></div>
    </div>

    <script type="text/javascript">
    function EnableDisableTextBox(kehadiran){
      var selectedValue = kehadiran.options[kehadiran.selectedIndex].value;
      var isiKehadiran = document.getElementById("isiKehadiran");
      var alasan = document.getElementById("alasan");
      isiKehadiran.disabled = selectedValue === "hadir" ? false : true;
      alasan.disabled = selectedValue === "berhalangan" ? false : true;
      if (!isiKehadiran.disabled){
        isiKehadiran.focus();
      }
    }

    </script>
</body>
</html>
@endsection