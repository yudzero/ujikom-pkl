<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\user;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['username' =>  '1234',
            'email' =>  'hubin@gmail.com',
            'level' =>  'hubin',
            'password'  =>  bcrypt('12345')  
             ],
            ['username' =>  '12345',
            'email' =>  'siswa@gmail.com',
            'level' =>  'siswa',
            'password'  =>  bcrypt('12345')  
             ],
            ['username' =>  '123456',
            'email' =>  'pembimbingsekolah@gmail.com',
            'level' =>  'pembimbing sekolah',
            'password'  =>  bcrypt('12345')  
            ],
            ['username' =>  '1234567',
            'email' =>  'pembimbingperusahaan@gmail.com',
            'level' =>  'pembimbing perusahaan',
            'password'  =>  bcrypt('12345')  
            ]
        ];

        foreach($users as $user){
            User::create($user);
        }
    }
}
