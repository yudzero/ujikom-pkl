<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siswas')->insert([
            'nis' => 271628,
            'id_jurusan'    =>  111,
            'nama' =>  Str::random(10),
            'kelas' =>  Str::random(10),
            'tgllahir'  => '04-12-2005',
            'Tmplahir' =>  Str::random(10),
            'Alamat_Siswa' =>  Str::random(10),
            'NoTelp'    =>  (08870614),
            'email' => Str::random(10).'@gmail.com',
        ]);
    }
}
