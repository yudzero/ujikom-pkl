<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elementkompetensi', function (Blueprint $table) {
            $table->id();
            $table->string('element');
            $table->unsignedBigInteger('id_jurusan');
            $table->string('tahun_ajar');
            $table->timestamps();

            $table->foreign('id_jurusan')->references('id_jurusan')->on('jurusan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemetaankompetensi');
    }
};
