<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemetaan', function (Blueprint $table) {
            $table->unsignedBigInteger('id_pendaftaran')->primary();
            $table->unsignedBigInteger('id_periode');
            $table->unsignedBigInteger('NoPerusahaan');
            $table->unsignedBigInteger('nis');
            $table->unsignedBigInteger('nip')->nullable();
            $table->enum('status', ['pending', 'ditolak', 'diterima']);
            $table->timestamps();

          
            
            $table->foreign('nip')->references('nip')->on('guru');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemetaan');
    }
};
