<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perusahaan', function (Blueprint $table) {
            $table->unsignedBigInteger('NoPerusahaan')->primary();
            $table->string('NamaPerusahaan');
            $table->longText('DeskripsiPerusahaan');
            $table->string('alamat');
            $table->string('fax');
            $table->string('email')->unique();
            $table->string('maps');
            $table->integer('kuota')->nullable();
            $table->boolean('status');
            $table->string('jurusan');
            $table->unsignedBigInteger('id_jurusan');
            $table->unsignedBigInteger('nip')->nullable();
            $table->timestamps();

            $table->foreign('id_jurusan')->references('id_jurusan')->on('jurusan');
            $table->foreign('nip')->references('nip')->on('guru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaan');
    }
};
