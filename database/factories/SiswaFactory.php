<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Siswa>
 */
class SiswaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nis' => fake()->unique()->randomNumber(),
            'id_jurusan'    => '111',
            'nama' => fake()->userName(),
            'kelas' => '11',
            'Tgllahir'  => fake()->date(),
            'Tmplahir'  => fake()->address(),
            'Alamat_Siswa'  =>  fake()->address(),
            'NoTelp'    =>  fake()->phoneNumber(),
            'email' =>  fake()->unique()->email()
        ];
    }
}
